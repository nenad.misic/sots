INSERT INTO user (first_name, last_name, username, password) VALUES ("Marko", "Markovic", "student1", "$2a$10$lQS4BxhQlNMUniNZZdomI.oMeod1Zn8ez9ORTsHe5vWe1Z46OQ8Se");
INSERT INTO user (first_name, last_name, username, password) VALUES ("Miloš", "Miloševic", "student2", "$2a$10$lQS4BxhQlNMUniNZZdomI.oMeod1Zn8ez9ORTsHe5vWe1Z46OQ8Se");
INSERT INTO user (first_name, last_name, username, password) VALUES ("Nikola", "Nikolic", "profesor1", "$2a$10$lQS4BxhQlNMUniNZZdomI.ufOw4VE41I4p2U61QGxQJGDVl.IE1kS");
INSERT INTO user (first_name, last_name, username, password) VALUES ("Dejan", "Dejanovic", "profesor2", "$2a$10$lQS4BxhQlNMUniNZZdomI.ufOw4VE41I4p2U61QGxQJGDVl.IE1kS");

INSERT INTO role (role_name) VALUES ("professor");
INSERT INTO role (role_name) VALUES ("student");

INSERT INTO user_role (u_id, r_id) VALUES (1,2);
INSERT INTO user_role (u_id, r_id) VALUES (2,2);
INSERT INTO user_role (u_id, r_id) VALUES (3,1);
INSERT INTO user_role (u_id, r_id) VALUES (4,1);

-- INSERT INTO test (test_name) VALUES ("Algebra");
-- INSERT INTO test (test_name) VALUES ("Osnove programiranja");

-- INSERT INTO question (content, test_id, points) VALUES ("Koliko je x u jednačini x*3 + 3 = 12?", 1, 2);
-- INSERT INTO question (content, test_id, points) VALUES ("Koliko je y u jednačini y*4 + 6 = 30?", 1, 3);
-- INSERT INTO question (content, test_id, points) VALUES ("Šta označava tip podataka integer?", 2, 3);
-- INSERT INTO question (content, test_id, points) VALUES ("Koja vrednost pripada tipu boolean?", 2, 4);

-- INSERT INTO answer (content, question_id) VALUES ("1", 1);
-- INSERT INTO answer (content, question_id) VALUES ("2", 1);
-- INSERT INTO answer (content, question_id) VALUES ("3", 1);
-- INSERT INTO answer (content, question_id) VALUES ("4", 1);
-- INSERT INTO answer (content, question_id) VALUES ("5", 1);
-- INSERT INTO answer (content, question_id) VALUES ("2", 2);
-- INSERT INTO answer (content, question_id) VALUES ("4", 2);
-- INSERT INTO answer (content, question_id) VALUES ("6", 2);
-- INSERT INTO answer (content, question_id) VALUES ("8", 2);
-- INSERT INTO answer (content, question_id) VALUES ("10", 2);
-- INSERT INTO answer (content, question_id) VALUES ("Tekstualni sadržaj", 3);
-- INSERT INTO answer (content, question_id) VALUES ("Jednocifreni broj", 3);
-- INSERT INTO answer (content, question_id) VALUES ("Ceo broj", 3);
-- INSERT INTO answer (content, question_id) VALUES ("Decimalni broj", 3);
-- INSERT INTO answer (content, question_id) VALUES ("Podatak vrednosti tačno/netačno", 3);
-- INSERT INTO answer (content, question_id) VALUES ("True", 4);
-- INSERT INTO answer (content, question_id) VALUES ("'tekst'", 4);
-- INSERT INTO answer (content, question_id) VALUES ("'t'", 4);
-- INSERT INTO answer (content, question_id) VALUES ("8", 4);
-- INSERT INTO answer (content, question_id) VALUES ("13.7", 4);

-- INSERT INTO correct_answer (answer_id, question_id) VALUES (3,1);
-- INSERT INTO correct_answer (answer_id, question_id) VALUES (8,2);
-- INSERT INTO correct_answer (answer_id, question_id) VALUES (13,3);
-- INSERT INTO correct_answer (answer_id, question_id) VALUES (16,4);

-- INSERT INTO professor_of (u_id, test_id) VALUES (3,1);
-- INSERT INTO professor_of (u_id, test_id) VALUES (4,1);
-- INSERT INTO professor_of (u_id, test_id) VALUES (3,2);

-- INSERT INTO student_of (u_id, test_id, solved, points) VALUES (1,1, false, 0);
-- INSERT INTO student_of (u_id, test_id, solved, points) VALUES (1,2, false, 0);
-- INSERT INTO student_of (u_id, test_id, solved, points) VALUES (2,2, false, 0);
