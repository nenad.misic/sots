var sqlite3 = require('sqlite3');
var open = require('sqlite').open;

var axios = require('axios')
const {ks_api} = require("../config/environment");

var constructGraph = async (iita_result) => {
    let implications = iita_result.implications;

    const db = await open({
        filename: '../database.db',
        driver: sqlite3.Database
    });

    let result = {
        problems: [],
        relations: []
    };

    for (let i = 0; i < implications.length; i++) {
        let relation = implications[i];

        var stmt = await db.prepare('SELECT * FROM problem WHERE id = @id', { '@id': relation[0] });
        let problem = await stmt.get();
        await stmt.finalize();

        result.problems.push(problem);

        stmt = await db.prepare('SELECT * FROM problem WHERE id = @id', { '@id': relation[1] });
        problem = await stmt.get();
        await stmt.finalize();

        result.problems.push(problem);

        result.relations.push({ child_id: relation[0], parent_id: relation[1] });
    }

    await db.close()

    result.problems = result.problems.filter((prob1, index, self) =>
        index === self.findIndex((prob2) => (
            prob1.id === prob2.id
        ))
    );

    // for (let i = 0; i < result.relations.length - 1; i++) {
    //     let rel1 = result.relations[i];
    
    //     for (let j = i + 1; j < result.relations.length; j++) {
    //         let rel2 = result.relations[j];
    
    //         if (rel1.parent_id === rel2.child_id && rel1.child_id === rel2.parent_id) {
    //             result.relations.splice(j, 1);
    //         }
    //     }
    // }

    return result;
}

var repository = {
    create: async (entity) => {
        const db = await open({
            filename: '../database.db',
            driver: sqlite3.Database
        });

        var stmt = await db.prepare('SELECT * FROM expec_ks WHERE test_id = @test_id', { '@test_id': entity.test_id });
        let expec_ks = await stmt.get();
        await stmt.finalize();

        if (expec_ks != null) {
            return;
        }

        expec_ks = await db.run('INSERT INTO expec_ks (test_id) VALUES (@test_id)',
                { '@test_id': entity.test_id });
        
        let promise_list = []
        for (const rel of entity.relations) {
            promise_list.push(db.run('INSERT INTO ks_relation (expec_ks_id, parent_id, child_id) VALUES (@expec_ks_id, @parent_id, @child_id)',
                { '@expec_ks_id': expec_ks.lastID, '@parent_id': rel[1], '@child_id': rel[0] }));
        }

        Promise.all(promise_list);

        await db.close();
        return true;
    },
    findById: async (id) => {
        const db = await open({
            filename: '../database.db',
            driver: sqlite3.Database
        });

        var stmt = await db.prepare('SELECT ksr.* FROM ks_relation ksr INNER JOIN expec_ks eks ON ksr.expec_ks_id = eks.id WHERE eks.test_id = @test_id', { '@test_id': id });
        let relations = await stmt.all();
        await stmt.finalize();

        if (relations.length === 0) {
            let url = `${ks_api}kspace/${id}`;
            let iita_result = (await axios.get(url)).data;

            return constructGraph(iita_result);
        }

        let problems = [];
        for (const rel of relations) {
            if (problems.find(p => p.id === rel.parent_id) == null) {
                stmt = await db.prepare('SELECT * FROM problem WHERE id = @id', { '@id': rel.parent_id });
                
                let problem = await stmt.get();
                await stmt.finalize();
                problems.push(problem);
            }
            if (problems.find(p => p.id === rel.child_id) == null) {
                stmt = await db.prepare('SELECT * FROM problem WHERE id = @id', { '@id': rel.child_id });

                let problem = await stmt.get();
                await stmt.finalize();
                problems.push(problem);
            }
        }

        let ks = {
            problems,
            relations
        };

        await db.close();
        return ks;
    }
};

module.exports = repository;