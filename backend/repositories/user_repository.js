var sqlite3 = require('sqlite3');
var open = require('sqlite').open;
var path = require('path');
 
// this is a top-level await 


var repository = {
    create: async (entity) => {
        // Inserts entity in db table, id should be missing and autogenerated
        const db = await open({
            filename: '../database.db',
            driver: sqlite3.Database
        });

        let result = await db.run('INSERT INTO user (username, password, first_name, last_name) VALUES (@username, @password, @first_name, @last_name)', 
        { '@username': entity.username, '@password': entity.password, '@first_name': entity.first_name, '@last_name': entity.last_name });
        
        await db.close()
        return result;
    },
    update: (entity) => {
        // Updates entity of given id in db table
    },
    delete: (id) => {
        // Deletes entity of given id from db table
    },
    exists: (id) => {
        // Returns True if entity with given id exists in db table, false otherwise
    },
    count: () => {
        // Returns total number of entities in db table
    },
    findById: (id) => {
        // Returns entity with given id from db table, null if not existent
    },
    findByUsername: async (username) => {
        // Returns entity with given username from db table, null if not existent
        const db = await open({
              filename: '../database.db',
              driver: sqlite3.Database
        });
        
        const stmt = await db.prepare('SELECT * FROM user WHERE username = @username')
        
        await stmt.bind({ '@username': username });

        let result = await stmt.get();
        await stmt.finalize();
        
        await db.close()
        return result;
    },
    getAll: () => {
        // Returns all entites from db table
    },
    getRoleByName: async (name) => {
        const db = await open({
            filename: '../database.db',
            driver: sqlite3.Database
        });
        
        const stmt = await db.prepare('SELECT * FROM role WHERE role_name = @name')
        
        await stmt.bind({ '@name': name });

        let result = await stmt.get();

        await stmt.finalize();
        
        await db.close()
        return result;
    },
    addRoleToUser: async (user_id, role_id) => {
        const db = await open({
            filename: '../database.db',
            driver: sqlite3.Database
        });

        let result = await db.run('INSERT INTO user_role (u_id, r_id) VALUES (@user_id, @role_id);', 
        { '@user_id': user_id, '@role_id': role_id });

        await db.close();
        return result;
    },
    getRolesForUser: async (id) => {
        const db = await open({
            filename: '../database.db',
            driver: sqlite3.Database
        });
        
        const stmt = await db.prepare('SELECT role_name FROM role WHERE id=(SELECT r_id FROM user_role WHERE u_id = @uid)')
        
        await stmt.bind({ '@uid': id });

        let result = await stmt.get();

        await stmt.finalize();
        
        await db.close()
        return result;
    },
    getStudents: async () => {
        const db = await open({
            filename: '../database.db',
            driver: sqlite3.Database
        });
        
        const stmt = await db.prepare('SELECT user.id, user.username, user.first_name, user.last_name FROM user INNER JOIN user_role ON user.id = user_role.u_id INNER JOIN role on user_role.r_id = role.id WHERE role.role_name = "student";')
        let result = await stmt.all();
        await stmt.finalize();
        
        await db.close()
        return result;
    },
    getProfessors: async () => {
        const db = await open({
            filename: '../database.db',
            driver: sqlite3.Database
        });
        
        const stmt = await db.prepare('SELECT user.id, user.username, user.first_name, user.last_name FROM user INNER JOIN user_role ON user.id = user_role.u_id INNER JOIN role on user_role.r_id = role.id WHERE role.role_name = "professor";')
        let result = await stmt.all();
        await stmt.finalize();
        
        await db.close()
        return result;
    },
}

module.exports = repository;