var answer_repository = require('./answer_repository');
var question_repository = require('./question_repository');
var test_repository = require('./test_repository');
var correct_answer_repository = require('./correct_answer_repository');
var user_repository = require('./user_repository');
var domain_repository = require('./domain_repository');
var problem_repository = require('./problem_repository');
var problem_relation_repository = require('./problem_relation_repository');
var ks_repository = require('./ks_repository');
 
module.exports = {
    answer_repository,
    question_repository,
    test_repository,
    user_repository,
    correct_answer_repository,
    domain_repository,
    problem_repository,
    problem_relation_repository,
    ks_repository
};