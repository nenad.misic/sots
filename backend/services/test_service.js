var axios = require('axios')
const {ks_api} = require("../config/environment");

var repositories = require('../repositories')

var service = {
    create: (entity) => {
        // Validates entity and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!entity.test_name || entity.test_name === "")
            throw new Error("Test name cannot be empty");
        else if (!entity.questions || entity.questions.lenght === 0)
            throw new Error("Test must have at least one question");

        return repositories.test_repository.create(entity);
    },
    solve: async (entity) => {
        if (!entity.result.test)
            throw new Error("Test is undefined");
        else if (!entity.result.test.questions || entity.result.test.questions.lenght === 0)
            throw new Error("Test answers are missing");
        
        for(let i = 0; i < entity.result.test.questions.lenght; i++) {
            const question = entity.result.test.questions[i];

            if (!question || !question.id || !question.answered || question.answered.lenght === 0)
                throw new Error("Some of the questions were not answered");
        }

        let result = await repositories.test_repository.solve(entity);
        console.log('>>>>>>>>>>>>>>>>>>>>>>')
        console.log(JSON.stringify(entity.state_object, null, 2));
        entity.state_object.sort((x,y) => y.likelihood - x.likelihood);
        console.log(JSON.stringify(entity.state_object, null, 2));
        let value = entity.state_object[0].state.join(',');
        repositories.test_repository.updateStudentState(value, entity.user.id, entity.result.test.id);
        return result;
    },
    update: (entity) => {
        // Validates entity and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
    },
    delete: (id) => {
        // Validates id and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!id)
            throw new Error('ID cannot be null or undefined');

        return repositories.test_repository.delete(id);
    },
    exists: (id) => {
        // Validates id and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!id)
            throw new Error('ID cannot be null or undefined');

        return repositories.test_repository.exists(id);
    },
    count: () => {
        // Delegates to repository.
        return repositories.test_repository.count();
    },
    findById: (id) => {
        // Validates id and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!id)
            throw new Error('ID cannot be null or undefined');

        return repositories.test_repository.findById(id);
    },
    getAll: () => {
        // Delegates to repository.
        return repositories.test_repository.getAll();
    },
    getAllForProfessor: (u_id) => {
        // Delegates to repository.
        return repositories.test_repository.getAllForProfessor(u_id);
    },
    getAllForStudent: (u_id) => {
        // Delegates to repository.
        return repositories.test_repository.getAllForStudent(u_id);
    },
    getStudentResults: (id, studentid) => {
        return repositories.test_repository.getStudentResults(id, studentid);
    },
    getResults: (p_id, t_id) => {
        return repositories.test_repository.getTestResults(p_id, t_id);
    },
    getStudentState: (t_id, s_id) => {
        return repositories.test_repository.getStudentState(t_id, s_id);
    }
}

module.exports = service;

