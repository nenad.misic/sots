var repositories = require('../repositories')

let arraysEqual = (a,b) =>{
    if (a === b) return true;
    if (a == null || b == null) return false;
    if (a.length !== b.length) return false;
  
    a.sort();
    b.sort();
    for (var i = 0; i < a.length; ++i) {
      if (a[i] !== b[i]) return false;
    }
    return true;
  }

let checksubset = (arr1, arr2) => arr2.every((el) => {
    return arr1.indexOf(el) !== -1;
});

var getKnowledgeStateGraph = (domain) => {
    let graph = {
        nodes: domain.problems.map(e => { return { id: e.id, name: e.name } }),
        links: domain.relations.map(e => { return { source: e.child_id, target: e.parent_id } })
    }
    let graph2 = {
        nodes: [],
        links: []
    }

    let currid = (() => {let cur = 1; return () => cur++})();
    const getAllSubsets = 
    theArray => theArray.reduce(
        (subsets, value) => subsets.concat(
            subsets.map(set => [value,...set])
        ),
        [[]]
    );

    let comb = getAllSubsets(graph.nodes.map(e => e.id));
    let final = JSON.parse(JSON.stringify(comb));
    comb.forEach((e,indeks) => {
        for (let i of e) {
            for (let d of graph.links.filter(x => x.target == i)) {
                if (e.indexOf(d.source) == -1){
                    delete final[indeks]
                }
            }
        }
    })
    final = final.filter(e => e).map(e => e.sort()).map(e => { return {id: currid(), values:e, name: e.map(x => graph.nodes.find(y => y.id == x).name).join(','), prids: e.map(x => graph.nodes.find(y => y.id == x).id)}});

    let finalinks = [];
    for (let i in final) {
        for (let j in final) {
            if (final[i].values.length + 1 !== final[j].values.length) continue;
            if (final[i].values.filter(x => final[j].values.indexOf(x) == -1).length > 0) continue;
            finalinks.push({source: final[i].id, target: final[j].id})
        }
    }

    graph2 = {nodes: final, links: finalinks}
    return graph2;
}

var service = {
    getFirst: async (testId) => {
        let graph = getKnowledgeStateGraph((await repositories.domain_repository.getDomainForTest(testId)));
        let student_states = await repositories.test_repository.getStudentStates(testId);
        let student_states_orig = JSON.parse(JSON.stringify(student_states));
        for(let i of graph.nodes) {
            if (student_states.find(e => arraysEqual(i.prids, e.student_state==''?[]:e.student_state.split(',').map(y=>+y)))){
                student_states.find(e => arraysEqual(i.prids, e.student_state==''?[]:e.student_state.split(',').map(y=>+y))).count += 1;
            } else{
                student_states.push({count: 1, student_state:i.prids.join(',')})
            }
        }
        // console.log(student_states);
        // let firstone = student_states.length == 0;
        let firstone = student_states.filter(x => x.count > 1).length <= 0;
        let so = graph.nodes.map(e => {return {state:e.prids,likelihood:firstone?1/graph.nodes.length:student_states.find(x=>arraysEqual(x.student_state==''?[]:x.student_state.split(',').map(y=>+y) ,e.prids)).count/(graph.nodes.length + student_states_orig.length)}});
        let questions = await repositories.question_repository.getAllForTest(testId);
        for (let q of questions) {
            q.value = so.filter(e => e.state.indexOf(q.problem_id) != -1).map( sasa => sasa.likelihood).reduce(((le,be) => le + be), 0);
        }


        sum = questions.map(q => q.value).reduce((a,b) => a + b);
        questions.forEach(q => q.value = q.value/sum);
        let rand = Math.random();
        let chosen = null;
        for(let q of questions){
            if (rand < q.value){
                chosen = q;
                break;
            }else{
                rand -= q.value;
            }
        }
        console.log(chosen);


        return {
            next_question: (await repositories.question_repository.findById(chosen.id)),
            state_object: so
        }
    },
    getNext: async (testId, data) => { // data = {id, answers, state_object, prev_questions}
    console.log(JSON.stringify(data,null,2));
        let correct = await repositories.question_repository.checkQuestion(data.id, data.answers);
        let q = await repositories.question_repository.findById(data.id);
        console.log(correct);
        if (correct) {
            for (let i of data.state_object) {
                if(i.state.indexOf(q.problem_id) !== -1) {
                    i.likelihood *= 1.2;
                } else {
                    i.likelihood *= 0.8;
                }
            }
        } else {
            for (let i of data.state_object) {
                if(i.state.indexOf(q.problem_id) !== -1) {
                    i.likelihood *= 0.8;
                } else {
                    i.likelihood *= 1.2;
                }
            }
        }

        let sum = data.state_object.map(a => a.likelihood).reduce((a,b) => a + b);
        for (let i of data.state_object) {
            i.likelihood = i.likelihood/sum;
        }

        let questions = await repositories.question_repository.getAllForTest(testId);

        for (let q of questions) {
            q.value = data.state_object.filter(e => e.state.indexOf(q.problem_id) != -1).map( sasa => sasa.likelihood).reduce((le,be) => le + be);
        }
        questions = questions.filter( x=> data.prev_questions.indexOf(x.id) == -1 );
        sum = questions.map(q => q.value).reduce((a,b) => a + b);
        questions.forEach(q => q.value = q.value/sum);
        let rand = Math.random();
        let chosen = null;
        for(let q of questions){
            if (rand < q.value){
                chosen = q;
                break;
            }else{
                rand -= q.value;
            }
        }
        // questions.sort((x,y) => y.value - x.value);

        return {
            next_question: questions.length == 0?null:(await repositories.question_repository.findById(chosen.id)),
            state_object: data.state_object
        }
    },
    create: (entity) => {
        // Validates entity and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!entity.content || entity.content === '')
            throw new Error('Content of the answer is missing');
        else if (!entity.test_id)
            throw new Error('ID of the test to which the question belongs is missing');
        else if (!entity.answers || entity.answers.length === 0)
            throw new Error('Answers to the question ' + entity.content + ' are missing');
        else if (!entity.points || entity.points <= 0)
            throw new Error('Points of the question: \"' + entity.content + '\" cannot be equals or less than zero');

        return repositories.question_repository.create(entity);
    },
    update: (entity) => {
        // Validates entity and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!entity.content || entity.content === '')
            throw new Error('Content of the answer is missing');
        else if (!entity.id)
            throw new Error('ID of the question is missing');
        else if (!entity.test_id)
            throw new Error('ID of the test to which the question belongs is missing');
        else if (!entity.answers || entity.answers.length === 0)
            throw new Error('Answers to the question ' + entity.content + ' are missing');

        return repositories.question_repository.update(entity);
    },
    delete: (id) => {
        // Validates id and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!id)
            throw new Error('ID cannot be null or undefined');

        return repositories.question_repository.delete(id);
    },
    exists: (id) => {
        // Validates id and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!id)
            throw new Error('ID cannot be null or undefined');

        return repositories.question_repository.exists(id);
    },
    count: () => {
        // Delegates to repository.
        return repositories.question_repository.count();
    },
    findById: (id) => {
        // Validates id and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!id)
            throw new Error('ID cannot be null or undefined');

        return repositories.question_repository.findById(id);
    },
    getAll: () => {
        // Delegates to repository.
        return repositories.question_repository.getAll();
    },
}

module.exports = service;

