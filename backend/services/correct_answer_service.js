var repositories = require('../repositories')

var service = {
    create: (entity) => {
        // Validates entity and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!entity.answer_id || !entity.question_id)
            throw new Error('Answer ID or question ID is missing');
        else if (!entity.test_id)

        return repositories.correct_answer_repository.create(entity);
    },
    update: (entity) => {
        // Validates entity and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
    },
    delete: (id) => {
        // Validates id and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!id)
            throw new Error('ID cannot be null or undefined');

        return repositories.correct_answer_repository.delete(id);
    },
    exists: (id) => {
        // Validates id and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!id)
            throw new Error('ID cannot be null or undefined');

        return repositories.correct_answer_repository.exists(id);
    },
    count: () => {
        // Delegates to repository.
        return repositories.correct_answer_repository.count();
    },
    findById: (id) => {
        // Validates id and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!id)
            throw new Error('ID cannot be null or undefined');

        return repositories.correct_answer_repository.findById(id);
    },
    findByQuestion: (id) => {
        if (!id)
            throw new Error('ID cannot be null or undefined');

        return repositories.correct_answer_repository.findByQuestion(id);
    }
    ,
    getAll: () => {
        // Delegates to repository.
        return repositories.correct_answer_repository.getAll();
    },
}

module.exports = service;

