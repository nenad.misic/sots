var answer_service = require('./answer_service');
var question_service = require('./question_service');
var test_service = require('./test_service');
var correct_answer_service = require('./correct_answer_service');
var user_service = require('./user_service');
var domain_service = require('./domain_service');
var problem_service = require('./problem_service');
var problem_relation_service = require('./problem_relation_service');
var ks_service = require('./ks_service')
 
module.exports = {
    answer_service,
    question_service,
    test_service,
    user_service,
    correct_answer_service,
    domain_service,
    problem_service,
    problem_relation_service,
    ks_service
};