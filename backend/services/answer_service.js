var repositories = require('../repositories')

var service = {
    create: (entity) => {
        // Validates entity and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!entity.content || entity.content === '')
            throw new Error('Content of the answer is missing');
        else if (!entity.question_id)
            throw new Error('ID of the question to which the answer belongs is missing');

        return repositories.answer_repository.create(entity);
    },
    update: (entity) => {
        // Validates entity and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!entity.content || entity.content === '')
            throw new Error('Content of the answer is missing');
        else if (!entity.id)
            throw new Error('ID of the answer is missing');

        return repositories.answer_repository.update(entity);
    },
    delete: (id) => {
        // Validates id and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!id)
            throw new Error('ID cannot be null or undefined');

        return repositories.answer_repository.delete(id);
    },
    exists: (id) => {
        // Validates id and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!id)
            throw new Error('ID cannot be null or undefined');

        return repositories.answer_repository.exists(id);
    },
    count: () => {
        // Delegates to repository.
        return repositories.answer_repository.count();
    },
    findById: (id) => {
        // Validates id and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!id)
            throw new Error('ID cannot be null or undefined');

        return repositories.answer_repository.findById(id);
    },
    getAll: () => {
        // Delegates to repository.
        return repositories.answer_repository.getAll();
    },
}

module.exports = service;
