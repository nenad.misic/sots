var repositories = require('../repositories')


function cycleHelper (node, originalId, dataset) {
    if (node.id === originalId) {
        return true;
    }
    if (!originalId) {
        originalId = node.id;
    }
    return dataset.links.filter(x=>x.source == node.id).map(async (x)=> cycleHelper(dataset.nodes.find(y => y.id == x.target), originalId, dataset)).filter(e => e===true).length > 0;
}

async function cyclicGraph (src, dst) {
    let q1 = await repositories.problem_repository.findById(src);
    let data = await repositories.domain_repository.findById(q1.domain_id)
    let dataset = {
        nodes: data.problems.map(e => {return  { id: e.id, name: e.name}}),
        links: data.relations.map(e => { return { _id: e.id, source: e.child_id, target: e.parent_id}})
    }
    dataset.links.push({source: src, target: dst})
    
    for (let node of dataset.nodes) {
      if (cycleHelper(node, null, dataset)) {
          return true;
      }
    }
    return false;
}


var service = {

    create: async (entity) => {
        // Validates entity and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!entity.parent_id || !entity.child_id)
            throw new Error('The relations must have two ends');

        if (await cyclicGraph(entity.child_id, entity.parent_id)){
            throw new Error('The graph is cyclic');
        }
            
        return repositories.problem_relation_repository.create(entity);
    },
    update: (entity) => {
        // Validates entity and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!entity.parent_id || !entity.child_id)
            throw new Error('The relations must have two ends');

        return repositories.problem_relation_repository.update(entity);
    },
    delete: (id) => {
        // Validates id and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!id)
            throw new Error('ID cannot be null or undefined');

        return repositories.problem_relation_repository.delete(id);
    },
    exists: (id) => {
        // Validates id and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!id)
            throw new Error('ID cannot be null or undefined');

        return repositories.problem_relation_repository.exists(id);
    },
    count: () => {
        // Delegates to repository.
        return repositories.problem_relation_repository.count();
    },
    findById: (id) => {
        // Validates id and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!id)
            throw new Error('ID cannot be null or undefined');

        return repositories.problem_relation_repository.findById(id);
    },
    getAll: () => {
        // Delegates to repository.
        return repositories.problem_relation_repository.getAll();
    },
}

module.exports = service;
