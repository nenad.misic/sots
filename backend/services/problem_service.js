var repositories = require('../repositories')

var service = {
    create: (entity) => {
        // Validates entity and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!entity.name || entity.name === '')
            throw new Error('Problem name is missing');
        else if (!entity.domain_id)
            throw new Error('Domain to which the problem \"' + entity.name + '\" belongs is missing');

        return repositories.problem_repository.create(entity);
    },
    update: (entity) => {
        // Validates entity and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!entity.name || entity.name === '')
            throw new Error('Problem name is missing');
        else if (!entity.domain_id)
            throw new Error('Domain to which the problem \"' + entity.name + '\" belongs is missing');

        return repositories.problem_repository.update(entity);
    },
    delete: (id) => {
        // Validates id and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!id)
            throw new Error('ID cannot be null or undefined');

        return repositories.problem_repository.delete(id);
    },
    exists: (id) => {
        // Validates id and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!id)
            throw new Error('ID cannot be null or undefined');

        return repositories.problem_repository.exists(id);
    },
    count: () => {
        // Delegates to repository.
        return repositories.problem_repository.count();
    },
    findById: (id) => {
        // Validates id and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!id)
            throw new Error('ID cannot be null or undefined');

        return repositories.problem_repository.findById(id);
    },
    getProblemsForDomain: (id) => {
        // Validates id and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!id)
            throw new Error('ID cannot be null or undefined');

        return repositories.problem_repository.getProblemsForDomain(id);
    },
    getAll: () => {
        // Delegates to repository.
        return repositories.problem_repository.getAll();
    },
}

module.exports = service;
