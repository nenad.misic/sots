var repositories = require('../repositories')

var service = {
    create: (entity) => {
        // Validates entity and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!entity.name || entity.name === '')
            throw new Error('Domain name is missing');
        else if (!entity.description || entity.description === '')
            throw new Error('Domain description is missing');

        return repositories.domain_repository.create(entity);
    },
    update: (entity) => {
        // Validates entity and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!entity.name || entity.name === '')
            throw new Error('Domain name is missing');
        else if (!entity.description || entity.description === '')
            throw new Error('Domain description is missing');

        return repositories.domain_repository.update(entity);
    },
    delete: (id) => {
        // Validates id and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!id)
            throw new Error('ID cannot be null or undefined');

        return repositories.domain_repository.delete(id);
    },
    exists: (id) => {
        // Validates id and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!id)
            throw new Error('ID cannot be null or undefined');

        return repositories.domain_repository.exists(id);
    },
    count: () => {
        // Delegates to repository.
        return repositories.domain_repository.count();
    },
    findById: (id) => {
        // Validates id and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!id)
            throw new Error('ID cannot be null or undefined');

        return repositories.domain_repository.findById(id);
    },
    getAll: () => {
        // Delegates to repository.
        return repositories.domain_repository.getAll();
    },
}

module.exports = service;
