var repositories = require('../repositories')

var service = {
    create: (entity) => {
        // Validates entity and passes it to repository if valid. Throws exception if not valid >>> throw new Error('Error description'); <<<.
        if (!entity.test_id)
            throw new Error("Test ID cannot be null or undefined");
        else if (!entity.relations || entity.relations.length === 0)
            throw new Error("Links cannot be null and at least one link must exists");

        return repositories.ks_repository.create(entity);
    },
    findById: async (id) => {
        if (!id)
            throw new Error('ID cannot be null or undefined');

        let result = await repositories.ks_repository.findById(id);
        return result;
    }
};

module.exports = service;