var express = require('express')
var services = require('../services')
var router = express.Router()

router.get('/', async function (req, res, next) {
    // Get all, delegate to service
    let problems = await services.problem_service.getAll();

    res.status(200).json(problems);
});

router.get('/exists/:id', async function (req, res, next) {
    // Exists by id (request.params.id), delegate to service
    let exists = await services.problem_service.exists(req.params.id);

    if (!exists)
        throw new Error('Problem with id ' + req.params.id + ' doesn\'t exist');

    res.status(200).json({ exists });
});

router.get('/count', async function (req, res, next) {
    // Get entity count, delegate to service
    let count = await services.problem_service.count();

    res.status(200).json({ count: count });
});

router.delete('/:id', async function (req, res, next) {
    // Delete by id (request.params.id), delegate to service
    let result = await services.problem_service.delete(req.params.id);

    res.status(200).json(result);
});

router.get('/:id', async function (req, res, next) {
    // Find by id (request.params.id), delegate to service
    let problem = await services.problem_service.findById(req.params.id);

    res.status(200).json(problem);
});

router.get('/domain/:id', async function (req, res, next) {
    // Find by id (request.params.id), delegate to service
    let problems = await services.problem_service.getProblemsForDomain(req.params.id);

    res.status(200).json(problems);
});

router.post('/', async function (req, res, next) {
    // Create new, delegate to service
    let result = await services.problem_service.create(req.body);

    res.status(201).json(result);
});

router.put('/', async function (req, res, next) {
    // Update existing, delegate to service
    let result = await services.problem_service.update(req.body);

    res.status(200).json(result);
});

module.exports = router