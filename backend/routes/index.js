var answer_routes = require('./answer_routes');
var correct_answer_routes = require('./correct_answer_routes');
var question_routes = require('./question_routes');
var test_routes = require('./test_routes');
var user_routes = require('./user_routes');
var domain_routes = require('./domain_routes');
var problem_routes = require('./problem_routes');
var problem_relation_routes = require('./problem_relation_routes');
var ks_routes = require('./ks_routes')
 
module.exports = {
    answer_routes,
    correct_answer_routes,
    question_routes,
    test_routes,
    user_routes,
    domain_routes,
    problem_routes,
    problem_relation_routes,
    ks_routes
};