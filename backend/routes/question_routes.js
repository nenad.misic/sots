var express = require('express')
var services = require('../services')
var router = express.Router()

router.get('/', async function (req, res, next) {
    // Get all, delegate to service
    let questions = await services.question_service.getAll();

    res.status(200).json(questions);
});

router.get('/first/:testId', async function (req, res, next) {
    // Get all, delegate to service
    let question = await services.question_service.getFirst(req.params.testId);
    res.status(200).json(question);
});

router.post('/next/:testId', async function (req, res, next) {
    // Get all, delegate to service
    let question = await services.question_service.getNext(req.params.testId, req.body);
    res.status(200).json(question);
});

router.get('/exists/:id', async function (req, res, next) {
    // Exists by id (request.params.id), delegate to service
    let exists = await services.question_service.exists(req.params.id);

    if (!exists)
        throw new Error('Question with id ' + req.params.id + ' doesn\'t exist');

    res.status(200).json({ exists });
});

router.get('/count', async function (req, res, next) {
    // Get entity count, delegate to service
    let count = await services.question_service.count();

    res.status(200).json({ count: count });
});

router.delete('/:id', async function (req, res, next) {
    // Delete by id (request.params.id), delegate to service
    let result = await services.question_service.delete(req.params.id);

    res.status(200).json(result);
});

router.get('/:id', async function (req, res, next) {
    // Find by id (request.params.id), delegate to service
    let question = await services.question_service.findById(req.params.id);

    res.status(200).json(question);
});

router.post('/', async function (req, res, next) {
    // Create new, delegate to service
    let result = await services.question_service.create(req.body);

    res.status(201).json(result);
});

router.put('/', async function (req, res, next) {
    // Update existing, delegate to service
    let result = await services.question_service.update(req.body);

    res.status(200).json(result);
});

module.exports = router