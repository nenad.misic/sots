var express = require('express')
var services = require('../services')
var router = express.Router()

router.get('/:id', async function (req, res, next) {
    // Find by id (request.params.id), delegate to service
    let kspace = await services.ks_service.findById(req.params.id);

    res.status(200).json(kspace);
});

router.post('/', async function (req, res, next) {
    // Create new, delegate to service
    let result = await services.ks_service.create(req.body);

    res.status(201).json(result);
});

module.exports = router