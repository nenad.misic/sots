var express = require('express')
var router = express.Router()
const jwt = require('jsonwebtoken');
var services = require('../services')

var bcrypt = require('bcryptjs');
var salt = bcrypt.genSaltSync(10);

var environment = require('../config/environment')

router.get('/', async function (req, res, next) {
    // Get all, delegate to service
});
router.get('/student', async function (req, res, next) {
    // Get all students, delegate to service
    let students = await services.user_service.getStudents();
    res.status(200).json(students);
});
router.get('/professor', async function (req, res, next) {
    // Get all professors, delegate to service
    let professors = await services.user_service.getProfessors();
    res.status(200).json(professors);
});

router.get('/:id', function (req, res, next) {
    // Find by id (request.params.id), delegate to service
});

router.get('/exists/:id', function (req, res, next) {
    // Exists by id (request.params.id), delegate to service
});

router.get('/count', function (req, res, next) {
    // Get entity count, delegate to service
});

router.delete('/:id', function (req, res, next) {
    // Delete by id (request.params.id), delegate to service
});

router.post('/', function (req, res, next) {
    // Create new, delegate to service
});

router.put('/', function (req, res, next) {
    // Update existing, delegate to service
});


router.post('/login', async function (req, res, next) {
    // {'username': 'string', 'password': 'string'}
    let creds = req.body;
    let user = await services.user_service.findByUsername(creds.username);

    if (user == null)
        res.status(404).json('Invalid username or password');
    
    let valid = await bcrypt.compare(creds.password, user.password);
    if (!valid)
        throw new Error('Invalid password');

    delete user.password;
    user.roles = [(await services.user_service.getRolesForUser(user.id)).role_name];

    const token = jwt.sign(user, environment.secret_key);

    res.status(200).json({ token });
});


router.post('/register', async function (req, res, next) {
    // {'username': 'string', 'password': 'string', 'first_name': 'string', 'last_name': 'string', 'role': 'string (enum: professor, student)'}

    var { username, password, first_name, last_name, role } = req.body;
    
    var hash = bcrypt.hashSync(password, salt);

    // Store hash as user password in db (delegate to service)

    let result = services.user_service.create({username, password: hash, first_name, last_name, role})

    res.status(200).json({message: 'ok'})
    

});

module.exports = router