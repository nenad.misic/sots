var express = require('express')
var services = require('../services')
var router = express.Router()

router.get('/', async function (req, res, next) {
    // Get all, delegate to service
    if(!req.logged_user) throw new Error('Unauthorized - login required');
    let tests = null;
    if (req.logged_user.roles.indexOf('professor') !== -1) {
        tests = await services.test_service.getAllForProfessor(req.logged_user.id);
    }
    else if (req.logged_user.roles.indexOf('student') !== -1) {
        tests = await services.test_service.getAllForStudent(req.logged_user.id);
    }
    else {
        throw new Error('User has to be either student or professor to access tests');
    }

    res.status(200).json(tests);
});

router.get('/exists/:id', async function (req, res, next) {
    // Exists by id (request.params.id), delegate to service
    let exists = await services.test_service.exists(req.params.id);

    if (!exists)
        throw new Error('Test with id ' + req.params.id + ' doesn\'t exist');

    res.status(200).json({ exists });
});

router.get('/count', async function (req, res, next) {
    // Get entity count, delegate to service
    let count = await services.test_service.count();

    res.status(200).json({ count: count });
});

router.delete('/:id', async function (req, res, next) {
    // Delete by id (request.params.id), delegate to service
    let result = await services.test_service.delete(req.params.id);

    res.status(200).json(result);
});

router.get('/results/:id', async function (req, res, next) {
    // Get all professors, delegate to service
    if(!req.logged_user)
        throw new Error('Unauthorized - login required');    
    else if (req.logged_user.roles.indexOf('professor') !== -1) {
        let result = await services.test_service.getResults(req.logged_user.id, req.params.id);
        res.status(200).json(result);
    }
    else
        throw new Error('User has to be professor to access test results');
});

router.get('/testStudentResults/:id/:studentid', async function (req, res, next) {
    // Find by id (request.params.id), delegate to service
    console.log('hello')
    let test = await services.test_service.getStudentResults(req.params.id, req.params.studentid);

    res.status(200).json(test);
});

router.get('/testStudentState/:id/:studentid', async function (req, res, next) {
    // Find by id (request.params.id), delegate to service
    if(!req.logged_user)
        throw new Error('Unauthorized - login required');    
    let test = await services.test_service.getStudentState(req.params.id, req.params.studentid);

    res.status(200).json(test);
});

router.get('/:id', async function (req, res, next) {
    // Find by id (request.params.id), delegate to service
    let test = await services.test_service.findById(req.params.id);

    res.status(200).json(test);
});

router.post('/', async function (req, res, next) {
    // Create new, delegate to service
    let result = await services.test_service.create(req.body);

    res.status(201).json(result);
});

router.post('/solve', async function(req, res, next) {
    // Add user to the body
    req.body.user = req.logged_user;

    let result = await services.test_service.solve(req.body);

    res.status(200).json(result);
});

router.put('/', function (req, res, next) {
    // Update existing, delegate to service
});

module.exports = router