var express = require('express');
var path = require('path');
var logger = require('morgan');
var http = require('http');
const cors = require('cors');

var routes =  require('./routes');
var middlewares = require('./middlewares')

var app = express();


app.use(cors({ origin: 'http://localhost:3000' }));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(middlewares.auth_handler);

app.use(express.static(path.join(__dirname, 'public')));

app.use('/user', routes.user_routes);
app.use('/test', routes.test_routes);
app.use('/question', routes.question_routes);
app.use('/answer', routes.answer_routes);
app.use('/correct_answer', routes.correct_answer_routes);
app.use('/domain', routes.domain_routes);
app.use('/problem', routes.problem_routes);
app.use('/relation', routes.problem_relation_routes);
app.use('/kspace', routes.ks_routes);

app.get('/', (req, res, next) => {
    res.send('Success');
});

//app.use(middlewares.error_handler);

var server = http.createServer(app);
server.listen(process.env.PORT || 1337);
console.log(`Listening on port ${process.env.PORT || 1337}`);

module.exports = app;