import {api, bearer} from '../Environment/environment';
import axios from 'axios';

let testService = {
    getAll: () => {
        let url = `${api}test`;
        return axios.get(url, bearer());
    },
    getById: (id) => {
        let url = `${api}test/${id}`;
        return axios.get(url, bearer());
    },
    getResults: async (id) => {
        let url = `${api}test/results/${id}`;
        try {
            return await axios.get(url, bearer());
        } catch (error) {
            return [];
        }
    },
    addTest: (entity) => {
        let url = `${api}test/`;
        return axios.post(url, entity, bearer());

    },
    solveTest: (entity) => {
        let url = `${api}test/solve`;
        return axios.post(url, entity, bearer());
    },
    getStudentResults: (test_id, student_id) => {
        let url = `${api}test/testStudentResults/${test_id}/${student_id}`;
        return axios.get(url, bearer());
    },
    getStudentState: (test_id, student_id) => {
        let url = `${api}test/testStudentState/${test_id}/${student_id}`;
        return axios.get(url, bearer());
    }
};

export default testService;
