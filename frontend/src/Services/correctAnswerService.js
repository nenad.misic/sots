import {api, bearer} from '../Environment/environment';
import axios from 'axios';

let correctAnswerService = {
    getForQuestion: (qid) => {
        let url = `${api}correct_answer/question/${qid}`;
        return axios.get(url, bearer());
    }
    
};

export default correctAnswerService;
