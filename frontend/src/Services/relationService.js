import {api, bearer} from '../Environment/environment';
import axios from 'axios';

let problemService = {
    create: (entity) => {
        let url = `${api}relation`;
        return axios.post(url, entity, bearer());
    },
    update: (entity) => {
        let url = `${api}relation`;
        return axios.put(url, entity, bearer());
    },
    delete: (id) => {
        let url = `${api}relation/${id}`;
        return axios.delete(url, bearer());
    },
    findById: (id) => {
        let url = `${api}relation/${id}`;
        return axios.get(url, bearer());
    },
    getAll: () => {
        let url = `${api}relation`;
        return axios.get(url, bearer());
    }
};

export default problemService;
