import {api, bearer} from '../Environment/environment';
import axios from 'axios';

let domainService = {
    create: (entity) => {
        let url = `${api}domain`;
        return axios.post(url, entity, bearer());
    },
    update: (entity) => {
        let url = `${api}domain`;
        return axios.put(url, entity, bearer());
    },
    delete: (id) => {
        let url = `${api}domain/${id}`;
        return axios.delete(url, bearer());
    },
    findById: (id) => {
        let url = `${api}domain/${id}`;
        return axios.get(url, bearer());
    },
    getAll: () => {
        let url = `${api}domain`;
        return axios.get(url, bearer());
    }
};

export default domainService;
