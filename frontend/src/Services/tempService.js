
import {api, bearer} from '../Environment/environment';
import axios from 'axios';

let tempService = {
    post_method: (param1, param2, obj) => {
        let url = `${api}endpoint/${param1}/${param2}`;
        return axios.post(url, obj, bearer());
    },
    get_method: (param1, param2) => {
        let url = `${api}endpoint/${param1}/${param2}`;
        return axios.get(url, bearer());
    }
};

export default tempService

