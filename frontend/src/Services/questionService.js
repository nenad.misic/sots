import {api, bearer} from '../Environment/environment';
import axios from 'axios';

let questionService = {
    getFirst: async (id) => {
        let url = `${api}question/first/${id}`;
        return axios.get(url, bearer());
    },
    getNext: async (id, data) => {
        let url = `${api}question/next/${id}`;
        return axios.post(url, data, bearer());
    }
};

export default questionService;
