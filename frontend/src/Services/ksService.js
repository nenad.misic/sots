import {api, bearer} from '../Environment/environment';
import axios from 'axios';

let ksService = {
    create: (entity) => {
        let url = `${api}kspace`;
        return axios.post(url, entity, bearer());
    },
    findById: (id) => {
        let url = `${api}kspace/${id}`;
        return axios.get(url, bearer());
    }
};

export default ksService;