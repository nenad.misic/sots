import {api, bearer} from '../Environment/environment';
import axios from 'axios';

let problemService = {
    create: (entity) => {
        let url = `${api}problem`;
        return axios.post(url, entity, bearer());
    },
    update: (entity) => {
        let url = `${api}problem`;
        return axios.put(url, entity, bearer());
    },
    delete: (id) => {
        let url = `${api}problem/${id}`;
        return axios.delete(url, bearer());
    },
    findById: (id) => {
        let url = `${api}problem/${id}`;
        return axios.get(url, bearer());
    },
    getProblemsForDomain: (id) => {
        let url = `${api}problem/domain/${id}`;
        return axios.get(url, bearer());
    },
    getAll: () => {
        let url = `${api}problem`;
        return axios.get(url, bearer());
    }
};

export default problemService;
