import React, { useState } from "react";
import { Button, FormGroup, FormControl } from "react-bootstrap";
import userService from '../Services/userService';
import { useHistory } from "react-router-dom";
import toast from "../Shared/Toast";

export default function Login() {
  let history = useHistory()
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  function validateForm() {
    return username.length > 0 && password.length > 0;
  }

  function handleSubmit(event) {
    event.preventDefault();
    let res = userService.login(username, password).then(res => {
      if (res)
        history.push('/');
      else
        toast.error('Invalid credentials');
    }).catch(err => console.log(err));
  }

  return (
    <div className="Login">
      <form onSubmit={handleSubmit}>
        <FormGroup controlId="username" bssize="large">
          <label>Username</label>
          <FormControl
            autoFocus
            type="username"
            value={username}
            onChange={e => setUsername(e.target.value)}
          />
        </FormGroup>
        <FormGroup controlId="password" bssize="large">
          <label>Password</label>
          <FormControl
            value={password}
            onChange={e => setPassword(e.target.value)}
            type="password"
          />
        </FormGroup>
        <Button className="my-5" block bssize="large" disabled={!validateForm()} type="submit">
          Login
        </Button>
      </form>
    </div>
  );
}
