import React, { Component } from 'react';
import domainService from '../Services/domainService';
import { Redirect } from 'react-router-dom';
import toast from "../Shared/Toast";

class AddDomain extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            description: ''
        };
    }

    handleInputChange(e) {
        const name = e.target.name;
        const value = e.target.value;

        this.setState({
            [name]: value
        });
    }

    everythingOkay() {
        return this.state.name !== '' && this.state.description !== ''
    }

    async handleSubmit(e) {
        e.preventDefault();

        await domainService.create(this.state).then(e => toast.info('New domain successfully saved')).catch(e => toast.error(e))
        this.props.history.push('/domains')
    }

    render() {
        return (
            <div className="offset-lg-3 offset-md-1 col-lg-6 col-md-10 col-12">
                <form onSubmit={(e) => this.handleSubmit(e)}>
                    <div className="form-group">
                        <label htmlFor="name">Domain name</label>
                        <input type="text" className="form-control" id="name" aria-describedby="domainNameHelp" placeholder="Enter domain name" name="name" onChange={(e) => this.handleInputChange(e)}/> 
                    </div>
                    <div className="form-group">
                        <label htmlFor="description">Domain description</label>
                        <input type="text" className="form-control" id="description" aria-describedby="domainDescriptionHelp" placeholder="Enter domain description" name="description" onChange={(e) => this.handleInputChange(e)}/> 
                    </div>
                    <button type="submit" disabled={!this.everythingOkay()} className="btn btn-success col-8 offset-2 my-5">Add domain</button>
                </form>
            </div>
        );
    }
}

export default AddDomain;
