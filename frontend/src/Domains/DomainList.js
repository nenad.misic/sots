import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import domainService from '../Services/domainService';
import { Redirect } from 'react-router-dom'
import toast from "../Shared/Toast";

class DomainList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            domains: []
        };
    }

    async componentDidMount() {
        let domains = (await domainService.getAll()).data;
        this.setState({ domains });
    }

    deleteDomain(id) {
        domainService.delete(id).then(() => {
            let ds = JSON.parse(JSON.stringify(this.state.domains));
            ds.splice(ds.findIndex(domain => domain.id === id), 1);

            this.setState({
                domains: ds
            });
            toast.info('Domain successfully deleted')
        }).catch(e => toast.error(e));
    }

    create() {
        
    }

    render() {
        if (!localStorage.getItem('identity')) {
            return <Redirect to='/unauthorized' />
        }
        return (
            <div>
                <div className="p-3 list-group col-lg-6 col-md-8 col-sm-12 offset-lg-3 offset-md-2">
                    <div className="d-flex justify-content-start">
                        <Link to="/addDomain" className="my-3 btn btn-info btn-md">Add</Link>
                    </div>
                    {!this.state.domains && (
                        <div className="h1"> Loading... </div>
                    )}
                    {this.state.domains && this.state.domains.length === 0 && (
                        <div className="h1"> No available domains! </div>
                    )}
                    {this.state.domains && this.state.domains.length !== 0 && (
                        this.state.domains.map(e => (
                            <div key={e.id} className="my-4 mly list-group-item flex-column align-items-start">
                                <div className="d-flex w-100 justify-content-between">
                                    <h5 className="mb-1">{e.name}</h5>
                                </div>
                                <hr></hr>
                                <p className="mt-3 mb-1">
                                    {e.description}
                                </p>
                                <div className="d-flex w-100 justify-content-between mt-4">
                                    <div>
                                        <div><small>{e.problems.length} problem(s)</small></div>
                                    </div>
                                    <div className="d-flex justify-content-end">
                                        <div>
                                            {JSON.parse(localStorage.getItem('identity')).roles.includes('professor') && (
                                                <Link to={`/graph/${e.id}`} className="btn btn-info pointer">Manage</Link>
                                            )}
                                        </div>
                                        <div className="ml-2">
                                            {JSON.parse(localStorage.getItem('identity')).roles.includes('professor') && (
                                                <button className="btn btn-danger pointer" onClick={() => this.deleteDomain(e.id)}>Delete</button>
                                            )}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ))
                    )}
                </div>
            </div>
        );
    }
}

export default DomainList;
