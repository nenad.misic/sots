import React, { useState } from "react";
import { Button, FormGroup, FormControl } from "react-bootstrap";
import userService from '../Services/userService';
import { useHistory } from "react-router-dom";
import toast from "../Shared/Toast";

export default function Register() {
  let history = useHistory()
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [role, setRole] = useState("");

  function validateForm() {
    return username.length > 0 && firstName.length > 0 && lastName.length > 0 && password.length > 0 && role;
  }

  function handleSubmit(event) {
    event.preventDefault();
    if (userService.register(username, password, firstName, lastName, role.toLowerCase()))
    {
      history.push('/');
    } else {
        toast.error("Registration failed.")
    }
  }

 

  return (
    <div className="Login">
      <form onSubmit={handleSubmit}>
        <FormGroup controlId="username" bssize="large">
          <label>Username</label>
          <FormControl
            autoFocus
            type="username"
            value={username}
            onChange={e => setUsername(e.target.value)}
          />
        </FormGroup>
        <FormGroup controlId="firstName" bssize="large">
          <label>First name</label>
          <FormControl
            type="text"
            value={firstName}
            onChange={e => setFirstName(e.target.value)}
          />
        </FormGroup>
        <FormGroup controlId="lastName" bssize="large">
          <label>Last name</label>
          <FormControl
            type="text"
            value={lastName}
            onChange={e => setLastName(e.target.value)}
          />
        </FormGroup>
        <FormGroup controlId="password" bssize="large">
          <label>Password</label>
          <FormControl
            value={password}
            onChange={e => setPassword(e.target.value)}
            type="password"
          />
        </FormGroup>
        <FormGroup controlId="exampleForm.ControlSelect1" id="roleSelect" onChange={e => setRole(e.target.value)}>
            <label>Role</label>
            <FormControl as="select">
                <option>Student</option>
                <option>Professor</option>
            </FormControl>
        </FormGroup>
        <Button className="my-5" block bssize="large" disabled={!validateForm()} type="submit">
          Register
        </Button>
      </form>
    </div>
  );
}
