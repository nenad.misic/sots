module.exports.api = 'http://localhost:1337/';
module.exports.bearer = () => { return {
    headers: { Authorization: `Bearer ${localStorage.getItem('user-token')}` }
}}
