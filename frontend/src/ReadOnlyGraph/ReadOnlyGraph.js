import React, {Component} from 'react';
import domainService from '../Services/domainService';
import problemService from '../Services/problemService';
import relationService from '../Services/relationService';
import * as d3 from "d3";

class ReadOnlyGraph extends Component {

    constructor(props) {
        super(props);
        this.state = {
            dataset: null
        };
    }
    async componentDidMount() {
        debugger;
        let dataset = this.props.dataset;
        let w = this.props.width || 500;
        let h = this.props.height || 500;
        this.setState({dataset});
        this.drawChart(this, dataset, w,h);
    }

    drawChart(thatx, ds,w,h) {
        var dataset = ds;
        let margin = ({top: 30, right: 80, bottom: 30, left: 30}); 
        let width = w - margin.left - margin.right;
        let height = h;
        
        var div = d3.select("body").append("div")	
            .attr("class", "tooltip")				
            .style("opacity", 0);
        
        function rerenderer() {
            d3.select(`#${thatx.props.chartid}`).html('');
            
            let simulation = d3.forceSimulation()
                .force("link", d3.forceLink()
                                .id(d => d.id).distance(100)
                ) 
                .force("charge", d3.forceManyBody().strength(-250))
                .force("center", d3.forceCenter().x(width * .5).y(height * .5));

            const cont = d3.select(`#${thatx.props.chartid}`)
                .append("svg")
            
            cont
                .append('defs').append('marker')
                .attr("id",'arrowhead')
                .attr('viewBox','-0 -5 10 10') //the bound of the SVG viewport for the current SVG fragment. defines a coordinate system 10 wide and 10 high starting on (0,-5)
                .attr('refX',23) // x coordinate for the reference point of the marker. If circle is bigger, this need to be bigger.
                .attr('refY',0)
                .attr('orient','auto')
                    .attr('markerWidth',10)
                    .attr('markerHeight',10)
                    .attr('xoverflow','visible')
                .append('svg:path')
                .attr('d', 'M 0,-5 L 10 ,0 L 0,5')
                .attr('fill', '#999')
                .style('stroke','none');
            const svg = cont
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .call(d3.zoom().on("zoom", function (d) {
                    svg.attr("transform", d3.event.transform)
                }))
            .append("g")
                .attr("transform", `translate(${margin.left},${margin.top})`);

            svg.append('rect')
                .attr('x', -margin.left)
                .attr('y', -margin.top)
                .attr('width', width + margin.left + margin.right)
                .attr('height', height + margin.top + margin.bottom)
                .attr('fill', 'white');


            const link = svg.append("g")
                .attr("class", "links")
                .selectAll("line")
                .data(dataset.links)
            .enter().append("line")
            .attr('marker-end','url(#arrowhead)')
            ;

                
            const node = svg.append("g")
                .attr("class", "nodes")
                .selectAll("circle")
                .data(dataset.nodes)
            .enter().append("circle")
                .attr("class", "colored_circle")
                .style('fill', e => e.fillColor || '#1f9bcf')
                .attr("r", 15)
                .attr('id', d => `circle${d.id}`)
                .call(d3.drag()
                    .on("start", dragstarted)
                    .on("drag", dragged)
                    .on("end", dragended)
                );

            
            // Text to nodes
            const text = svg.append("g")
                .attr("class", "text")
                .selectAll("text")
                .data(dataset.nodes)
            .enter().append("text")
                .text(d => d.name)
                .attr('dx', 25)

            simulation
                .nodes(dataset.nodes)
                .on("tick", ticked);

            simulation.force("link")
                .links(dataset.links);

            function ticked() {
                link.attr("x1", d => {
                    return d.source.x
                })
                .attr("y1", d => {
                    return d.source.y
                })
                .attr("x2", d => {
                    return d.target.x
                })
                .attr("y2", d => {
                    return d.target.y
                });
    
                node.attr("cx", d => {
                    return d.x;
                })
                    .attr("cy", d => { 
                        return d.y;
                });
    
                node.attr("cx", d => d.x)
                    .attr("cy", d => d.y);
    
                text.attr("x", d => d.x - 5)
                    .attr("y", d => d.y + 5);
            }
    
            function dragstarted(d) {
                if (!d3.event.active) simulation.alphaTarget(0.3).restart();
                d.fy = d.y;
                d.fx = d.x;
            }
    
            function dragged(d) {
                d.fx = d3.event.x;
                d.fy = d3.event.y;
            }
    
            function dragended(d) {
                if (!d3.event.active) simulation.alphaTarget(0);
                d.fx = null;
                d.fy = null;
            }
        }
        rerenderer();
    }

    
    render() {
        return (
            <div id={this.props.chartid}>
            </div>
        )
    }

}

export default ReadOnlyGraph;