import React, { Component } from 'react';
import testService from '../Services/testService';
import { Redirect, Link } from 'react-router-dom';

class StudentResults extends Component {
    constructor(props) {
        super(props);

        this.state = {
            results: null
        }
    }

    async componentDidMount() {
        let results = (await testService.getResults(+this.props.match.params.testID)).data;
        this.setState({ results });
    }

    render() {
        if (!localStorage.getItem('identity')) {
            return <Redirect to='/unauthorized' />
        }

        return (
            <div>
                <div className="p-3 list-group col-lg-6 col-md-8 col-sm-12 offset-lg-3 offset-md-2">
                    {!this.state.results && (
                        <div className="h1"> Loading... </div>
                    )}
                    {this.state.results && this.state.results.length === 0 && (
                        <div className="h1"> No one solved this test! </div>
                    )}
                    {this.state.results && this.state.results.length !== 0 && (
                        this.state.results.map(e => (
                            <div key={e.id} className="my-4 mly list-group-item flex-column align-items-start">
                                <div className="d-flex w-100 justify-content-between">
                                    <h5 className="mb-1">{e.test_name} - {e.first_name} {e.last_name}</h5>
                                    <small>{e.date}</small>
                                </div>
                                <hr></hr>
                                <p className="mt-3 mb-1">
                                    {e.description}
                                </p>
                                <p className="mb-1">
                                    This is something that will be implemented eventually.
                                </p>
                                <div className="d-flex w-100 justify-content-between mt-4">
                                    <div>
                                        <div><small>{e.points} point(s)</small></div>
                                    </div>
                                    <div className="d-flex justify-content-end mt-4">
                                        {JSON.parse(localStorage.getItem('identity')).roles.includes('professor') && (
                                                <Link  to={`/testresults/${e.test_id}/${e.student_id}`} className="btn btn-info pointer mr-3">View answers</Link>
                                        )}
                                        {JSON.parse(localStorage.getItem('identity')).roles.includes('professor') && (
                                                <Link  to={`/kstate/${e.test_id}/${e.student_id}`} className="btn btn-info pointer ml-3">Knowledge state</Link>
                                        )}
                                    </div>
                                    
                                </div>
                            </div>
                        ))
                    )}
                </div>
            </div>
        );
    }
}

export default StudentResults;