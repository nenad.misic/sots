
import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import testService from '../Services/testService';
import correctAnswerService from '../Services/correctAnswerService';
import { Redirect } from 'react-router-dom'

class TestResults extends Component {
  constructor(props) {
    super(props);
    this.id = '';
    this.studentid = '';
    this.state = {
      test: null,
      student_results: null,
    };
  }

  async componentDidMount() {
      debugger;
    const { match: { params } } = this.props;
    this.id = params.testID;
    this.studentid = params.studentid || JSON.parse(localStorage.getItem('identity')).id;

    let test = (await testService.getById(this.id)).data;
    let student_results =  (await testService.getStudentResults(this.id, this.studentid)).data;

    this.setState({test,student_results});
  }

  render() {
    if (!localStorage.getItem('identity'))
    {
      return <Redirect to='/unauthorized' />
    }
    return (
        <div>
        {this.state.test && (
            <div className="mt-3 mb-5">

                <h2 className="ml-5 mb-5">{this.state.test.test_name}</h2>
                {this.state.test.questions.map((q,qi) => (
                    <div className="col-lg-8 col-10 offset-lg-2 offset-1 border my-3 py-2 px-4">
                        <div className="my-3"><h6 className="d-inline">{qi+1}. </h6><h4 className="d-inline">{q.content}</h4></div>
                        
                        {q.answers.map((a,ai) => (
                            <div className={`ml-5 my-1 wfc ${this.state.student_results.find(x => x.question_id === q.id) && this.state.student_results.find(x => x.question_id == q.id).caid.find(y =>  y === a.id)?'bg-success2 border border-success':''}`}>
                                <div className={`d-inline-block wh30 ${this.state.student_results.find(x => x.question_id == q.id) && this.state.student_results.find(x => x.question_id == q.id).aqid.find(y => y == a.id)?'border border-circular border-primary':''}`}>&nbsp;{ai+1}.&nbsp;</div> <p className="d-inline ml-3">{a.content}</p>
                            </div>
                        ))}
                    </div>
                ))}
            </div>
        )}
        </div>
    );
  }
  
}

export default TestResults;
