
import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import testService from '../Services/testService';
import { Redirect } from 'react-router-dom'

class TestList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tests: null
    };
  }

  async componentDidMount() {
    let tests = (await testService.getAll()).data;
    this.setState({tests});
  }

  render() {
    if (!localStorage.getItem('identity'))
    {
      return <Redirect to='/unauthorized' />
    }
    return (
        <div>
            <div className="p-3 list-group col-lg-6 col-md-8 col-sm-12 offset-lg-3 offset-md-2">
                <div className="d-flex justify-content-start">
                    {JSON.parse(localStorage.getItem('identity')).roles.includes('professor') && (
                        <Link to='/addTest' className="my-3 btn btn-info btn-md">Add</Link>
                    )}
                </div>
                {!this.state.tests && (
                    <div className="h1"> Loading... </div>
                )}
                {this.state.tests && this.state.tests.length == 0 && (
                    <div className="h1"> No available tests! </div>
                )}
                {this.state.tests && this.state.tests.length != 0 && (
                        this.state.tests.map(e => (
                            <div key={e.id} className="my-4 mly list-group-item flex-column align-items-start">
                                <div className="d-flex w-100 justify-content-between">
                                <h5 className="mb-1">{e.test_name}</h5>
                                <small>Created {e.date}</small>
                                </div>
                                <hr></hr>
                                <p className="mt-3 mb-1">
                                    {e.description}
                                </p>
                                <div className="d-flex w-100 justify-content-between mt-4">
                                    <div>
                                        <div><small>{e.questions.length} question(s)</small></div>
                                    </div>
                                    
                                    {JSON.parse(localStorage.getItem('identity')).roles.includes('student') && e.solved == true && (
                                        `${e.points}/${e.questions.map(q => q.points).reduce((a, b) => a + b, 0)} point(s)`
                                    )}
                                    
                                </div>
                                <div className="d-flex w-100 justify-content-end mt-4">
                                    {JSON.parse(localStorage.getItem('identity')).roles.includes('professor') && (
                                        <Link to={`/test/${e.id}/results`} className="btn btn-info pointer mx-2">Results</Link>
                                    )}
                                    {JSON.parse(localStorage.getItem('identity')).roles.includes('professor') && (
                                        <a href={`http://localhost:1337/${e.id}.zip`} className="btn btn-info pointer mx-2">Export to IMS QTI</a>
                                    )}
                                    {JSON.parse(localStorage.getItem('identity')).roles.includes('professor') && (
                                        <Link to={`/test/${e.id}/kspace`} className="btn btn-info pointer mx-2">Knowledge space</Link>
                                    )}
                                    {JSON.parse(localStorage.getItem('identity')).roles.includes('student') && e.solved == false && (
                                        <Link  to={`/test/${e.id}`} className="btn btn-info pointer mx-2">Solve test</Link>
                                    )}
                                    {JSON.parse(localStorage.getItem('identity')) && JSON.parse(localStorage.getItem('identity')).roles.includes('student') && e.solved == true && (
                                        <div className="d-flex justify-content-end">
                                            <Link  to={`/kstate/${e.id}/${JSON.parse(localStorage.getItem('identity')).id}`} className="btn btn-info pointer mr-3">Knowledge state</Link>
                                            <Link  to={`/testresults/${e.id}/${JSON.parse(localStorage.getItem('identity')).id}`} className="btn btn-info pointer ml-3">View answers</Link>
                                        </div>
                                    )}
                                </div>
                            </div> 
                        ))
                )}
            </div>
        </div>
    );
  }
  
}

export default TestList;
