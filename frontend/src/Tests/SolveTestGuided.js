import React, { Component } from 'react';
import testService from '../Services/testService';
import questionService from '../Services/questionService';
import domainService from '../Services/domainService';
import { Redirect } from 'react-router-dom'
import toast from "../Shared/Toast";

class SolveTestGuided extends Component {
    constructor(props) {
        super(props);
        this.result = {
            test: {
                id: null,
                questions: []
            }
        };
        this.state = {
            test: null,
            index: 1,
            current: null,
            state_object: null,
            prev_questions: []
        };
    }

    async componentDidMount() {
        
        if (!localStorage.getItem('identity') || !JSON.parse(localStorage.getItem('identity')).roles.includes('student')) {
            return;
        }

        const { match: { params } } = this.props;
        this.result.test.id = params.testID;

        let test = (await testService.getById(+this.result.test.id)).data;
        let domain = (await domainService.findById(test.domain_id)).data;
        await this.getFirst();

        if (test.solved)
            this.props.history.push('/tests');

        test.questions.forEach(e => {
            e.weight = domain.problems.filter(x => x.id == e.problem_id)[0].weight
        });
        test.questions.sort((a, b) => a.weight - b.weight);

        this.result.test.questions = test.questions.map(e => { return { id: e.id, answered: [] } })
        this.setState({ test });
    }

    checkboxClicked(q, a) {
        if (this.result.test.questions.filter(e => e.id === q)[0].answered.indexOf(a) == -1) {
            this.result.test.questions.filter(e => e.id === q)[0].answered.push(a);
        }
        else {
            this.result.test.questions.filter(e => e.id === q)[0].answered = this.result.test.questions.filter(e => e.id === q)[0].answered.filter(x => x != a);
        }
        this.setState(this.state);
    }

    async sumbitTest() {
        let data = {result: this.result, state_object: this.state.state_object};
        await testService.solveTest(data).then(() => {
            toast.info('Answers have been submitted');
        }).catch((err) => {
            toast.error(err);
        });
        this.props.history.push('/tests');
    }

    async getFirst() {
        let result = (await questionService.getFirst(this.result.test.id)).data;
        let current = result.next_question;
        let state_object = result.state_object;

        current.answered = [];

        this.setState({ current, state_object });
    }

    async getNext() {
        let index = this.state.index + 1;
        debugger;
        document.querySelectorAll('input[type=checkbox]').forEach(el => el.checked = false);

        let prev_questions = JSON.parse(JSON.stringify(this.state.prev_questions));
        prev_questions.push(this.state.current.id);

        let data = {
            id: this.state.current.id,
            answers: this.result.test.questions.filter(e => e.id === this.state.current.id)[0].answered,
            prev_questions,
            state_object: this.state.state_object
        };

        let result = (await questionService.getNext(this.state.test.id, data)).data;
        let current = result.next_question;
        let state_object = result.state_object;

        current.answered = [];

        this.setState({ index, current, state_object, prev_questions });
    }

    render() {
        if (!localStorage.getItem('identity') || !JSON.parse(localStorage.getItem('identity')).roles.includes('student')) {
            return <Redirect to='/unauthorized' />
        }
        return (
            <div>
                {/* <div>{JSON.stringify(this.result.test.questions)}</div> */}
                {this.state.test === null && (
                    <div className="display-5"> Loading... </div>
                )}
                {this.state.test === undefined && (
                    <div className="display-5"> Test with id {this.result.test.id} does not exist! </div>
                )}
                {this.state.test && (
                    <div className="p-3 list-group col-lg-8 col-md-10 col-sm-12 offset-lg-2 offset-md-1">
                        <div className="display-5 text-center">
                            {this.state.test.test_name}
                        </div>
                        <hr></hr>

                        <div className="p-3 list-group col-lg-8 col-md-12 col-sm-12 offset-lg-2">
                            <div className="list-group-item flex-column align-items-start">
                                <div className="d-flex w-100 justify-content-between">
                                    <h6 className="mb-1">Question no. {this.state.index}</h6>
                                    <div>
                                        <div>
                                            <h6>Points: {this.state.current.points}</h6>
                                            <h6>Difficulty: {this.state.current.weight}</h6>
                                        </div>
                                    </div>
                                </div>
                                <br></br>

                                <p className="mt-4 my-3 lead">
                                    {this.state.current.content}
                                </p>

                                <div className="p-3 list-group col-lg-8 col-md-10 col-sm-10 offset-lg-2 offset-md-1 offset-sm-1">
                                    {this.state.current.answers && this.state.current.answers.length > 0 && this.state.current.answers.map((a, j) => (
                                        <div key={j} className="list-group-item flex-column align-items-start">
                                            <div className="pointer form-check ">
                                                <input className="pointer form-check-input" type="checkbox" value="" id={`answerCheckbox${j}`} onClick={() => { this.checkboxClicked(this.state.current.id, a.id) }} />
                                                <label className="pointer ml-3 form-check-label w-100 " htmlFor={`answerCheckbox${j}`}>
                                                    <small>{j + 1}.</small> &nbsp;&nbsp; {a.content}
                                                </label>
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            </div>
                        </div>

                        {this.state.index < this.state.test.questions.length && (
                            <button className="btn btn-success col-lg-6 col-md-8 col-sm-10 offset-lg-3 offset-md-2 offset-sm-1" onClick={() => { this.getNext(); }} disabled={this.state.current.answered.length != 0}> Next </button>
                        )}
                        {this.state.index === this.state.test.questions.length && (
                            <button className="btn btn-success col-lg-6 col-md-8 col-sm-10 offset-lg-3 offset-md-2 offset-sm-1" onClick={async () => { this.sumbitTest(); }}> Submit test </button>
                        )}

                    </div>
                )}
            </div>
        );
    }
}

export default SolveTestGuided;
