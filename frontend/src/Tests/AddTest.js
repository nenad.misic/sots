
import React, {Component} from 'react';
import testService from '../Services/testService';
import userService from '../Services/userService';
import domainService from '../Services/domainService';
import { Redirect } from 'react-router-dom'
import Select from 'react-select'
import ReadOnlyGraph from '../ReadOnlyGraph/ReadOnlyGraph'
import toast from "../Shared/Toast";


class AddTest extends Component {
  constructor(props) {
    super(props);
    this.state = {
        professors: [],
        students: [],
        domains: [],
        selected_domain: undefined,
        problems: [],
        test: {
          test_name: '',
          description: '',
          professors: [],
          students: [],
          questions: [
            {
              content: '',
              points: 0,
              problem_id: undefined,
              answers: [
                {content: '', isCorrect: false},
                {content: '', isCorrect: false},
              ],
            }
          ],
        }
      };
  }

  addNewQuestion(event) {
    event.preventDefault();
    let x = JSON.parse(JSON.stringify(this.state));
    x.test.questions.push({
      content: '',
      points: 0,
      problem_id: undefined,
      answers: [
        {content: '', isCorrect: false},
        {content: '', isCorrect: false},
      ]
    });
    this.setState(x);
  }

  addNewAnswer(event,qi) {
    event.preventDefault();
    let x = JSON.parse(JSON.stringify(this.state));
    x.test.questions[qi].answers.push({content: '', isCorrect: false});
    this.setState(x);
  }

  async componentDidMount() {
    if (!localStorage.getItem('identity') || !JSON.parse(localStorage.getItem('identity')).roles.includes('professor'))
    {
      return;
    }

    let [students, professors, domains] = await Promise.all([userService.getStudents(), userService.getProfessors(), domainService.getAll()]);

    let x = JSON.parse(JSON.stringify(this.state));
    x.students = students.data;
    x.professors = professors.data.filter(e => e.id !=JSON.parse(localStorage.getItem('identity')).id );
    x.domains = domains.data;
    this.setState(x);

  }

  
  handleChange = (event) => {
    let x = JSON.parse(JSON.stringify(this.state));
    let nam = event.target.name;
    let val = event.target.value;
    x.test[nam] = val;
    this.setState(x);
  }

  handleChangeOfQuestion = (event, i) => {
    let val = event.target.value;
    let x = JSON.parse(JSON.stringify(this.state));
    x.test.questions[i].content = val;
    this.setState(x);
  }

  handleChangeOfQPoints = (event, i) => {
    let val = event.target.value;
    let x = JSON.parse(JSON.stringify(this.state));
    x.test.questions[i].points = val;
    this.setState(x);
  }

  handleChangeOfAnswer = (event, i, j) => {
    let val = event.target.value;
    let x = JSON.parse(JSON.stringify(this.state));
    x.test.questions[i].answers[j].content = val;
    this.setState(x);
  }

  handleProfessorChange = (event) => {
    let x = JSON.parse(JSON.stringify(this.state))
    x.test.professors = event?event.map(e => e.value):[];
    this.setState(x);
  }

  handleStudentChange = (event) => {
    let x = JSON.parse(JSON.stringify(this.state))
    x.test.students = event?event.map(e => e.value):[];
    this.setState(x);
  }

  handleDomainChange = (event) => {
    let x = JSON.parse(JSON.stringify(this.state))
    x.selected_domain = event?event.value:undefined;
    x.test.questions.forEach(question => {
      question.problem_id = undefined; // Object.assign({}, a);
    });
    x.problems = this.state.domains.find(domain => domain.id === x.selected_domain).problems.map((problem,pi)=> { return {value: problem.id, label: `${pi+1}. ${problem.name}`}});
    this.setState(x);
  }

  handleProblemChange = (event, i) => {
    let x = JSON.parse(JSON.stringify(this.state));
    x.test.questions[i].problem_id = event?event.value:undefined;
    this.setState(x);
  }

  handleSubmit = async (event) => {
    event.preventDefault();

    // Delegate adding to service
    let x = JSON.parse(JSON.stringify(this.state))

    let selected_problems = [];
    x.test.questions.forEach(q => selected_problems.push(q.problem_id));

    if ((new Set(selected_problems)).size !== selected_problems.length) {
      toast.error('You have selected some of these problems multiple times...');
      return;
    }

    x.test.professors.push(JSON.parse(localStorage.getItem('identity')).id)
    x.test.domain_id = x.selected_domain;
    await testService.addTest(x.test).then(e => toast.info('New test successfully saved')).catch(e => toast.error(e))
    this.props.history.push('/tests');
  }

  everythingOkay = () => {
      return this.state.test.test_name != '' &&
      this.state.test.questions.length >= 0 &&
      this.state.test.questions.filter(e => e.answers.length <= 1).length === 0 &&
      this.state.test.questions.filter(e => !e.content).length === 0 &&
      this.state.test.questions.map(e => e.answers.filter(a => !a.content).length).filter(x => x!==0).length === 0 &&
      this.state.test.questions.every(q => q.points > 0) &&
      this.state.selected_domain != null && this.state.test.questions.every(question => question.problem_id != null);
  }

  checkboxClicked(q,a) {
    let x = JSON.parse(JSON.stringify(this.state));
    // if(x.questions[q].correct_answers.filter(e => e.content===x.questions[q].answers[a].content).length > 0){
    //   x.questions[q].correct_answers = x.questions[q].correct_answers.filter(y => y.content!==x.questions[q].answers[a].content);
    // }else {
    //   x.questions[q].correct_answers.push(x.questions[q].answers[a]);
    // }
    x.test.questions[q].answers[a].isCorrect = !x.test.questions[q].answers[a].isCorrect;
    this.setState(x);
  }


  render() {
    if (!localStorage.getItem('identity') || !JSON.parse(localStorage.getItem('identity')).roles.includes('professor'))
    {
      return <Redirect to='/unauthorized' />
    }
    return (
      <div>
      {this.state.students.length === 0 && (
        <div className="h1">
          Loading...
          </div>
      )}
        {/* <div>{JSON.stringify(this.state.test)}</div> */}
        
      {this.state.selected_domain && (

        <div className="offset-3 my-4">
          <ReadOnlyGraph dataset={
            {
              nodes: this.state.domains.filter(e => e.id == this.state.selected_domain)[0].problems.map(e => {return  { id: e.id, name: e.name}}),
              links: this.state.domains.filter(e => e.id == this.state.selected_domain)[0].relations.map(e => { return { _id: e.id, source: e.child_id, target: e.parent_id}})
            }
          } chartid={'chart'} width={window.innerWidth * 6/12} height={300}></ReadOnlyGraph>
        </div>

      
      )}
      {this.state.test.questions.length > 0 && this.state.students.length > 0 && (
        
        <div className="offset-lg-3 offset-md-1 col-lg-6 col-md-10 col-12">
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label htmlFor="test_name">Test name</label>
            <input type="text" className="form-control" id="test_name" aria-describedby="testNameHelp" placeholder="Enter test name" name="test_name" onChange={this.handleChange}/> 
          </div>

          <div className="form-group">
            <label htmlFor="description">Test description</label>
            <input type="text" className="form-control" id="description" aria-describedby="testDescriptionHelp" placeholder="Enter test description" name="description" onChange={this.handleChange}/> 
          </div>

          <div className="form-group">
            <label htmlFor="professors">Professors</label>
            <br></br>
            <Select
              isMulti
              name="professors"
              options={this.state.professors.map((professor,pi)=> { return {value: professor.id, label: `${pi+1}. ${professor.first_name} ${professor.last_name} - ${professor.username} (${professor.id})`}})}
              className="basic-multi-select"
              classNamePrefix="select"
              onChange={this.handleProfessorChange}
            />
          </div>

          {/* <div className="form-group">
            <label htmlFor="students">Students</label>
            <br></br>
            <select className="selectpicker w-100 customselect" id="students" name="students" onChange={this.handleStudentChange} multiple data-live-search="true">
              {this.state.students.map((student,si) => (
                <option key={student.id} id={student.id}></option>
              ))}
            </select>
          </div> */}

          <div className="form-group">
            <label htmlFor="students">Students</label>
            <br></br>
            <Select
              isMulti
              name="students"
              options={this.state.students.map((student,si)=> { return {value: student.id, label: `${si+1}. ${student.first_name} ${student.last_name} - ${student.username} (${student.id})`}})}
              className="basic-multi-select"
              classNamePrefix="select"
              onChange={this.handleStudentChange}
            />
          </div>

          <div className="form-group">
            <label htmlFor="domains">Domains</label>
            <br></br>
            <Select
              name="domains"
              options={this.state.domains.map((domain,di)=> { return {value: domain.id, label: `${di+1}. ${domain.name} - ${domain.description}`}})}
              className="basic-multi-select"
              classNamePrefix="select"
              isDisabled={this.state.selected_domain?true:false}
              onChange={this.handleDomainChange}
            />
          </div>

          <div>

          <label>Questions</label>
          {this.state.test.questions.map((e,i) => (
            <div key={i} className="border px-5 py-4 mb-4">
              <div className="mb-4 d-flex w-100 justify-content-between">
                  <h6 className="mb-1">Question no. {i+1}</h6>
                  <div>
                      <div>
                          <h6> <input type="number" min="1" className="form-control" id={`qpoints_${i}`} placeholder="Enter question points" name={`qpoints_${i}`} onChange={(evt) => {this.handleChangeOfQPoints(evt, i)}}/></h6>
                      </div>
                  </div>
              </div>
              <div className="form-group">
                <input type="text" className="form-control" id={`qcont_${i}`} placeholder="Enter question text" name={`qcont_${i}`} onChange={(evt) => {this.handleChangeOfQuestion(evt, i)}}/> 
              </div>
              <div className="form-group">
                <label htmlFor="problems">Problems</label>
                <br></br>
                <Select
                  name="problems"
                  options={this.state.selected_domain && this.state.problems}
                  className="basic-multi-select"
                  classNamePrefix="select"
                  onChange={(event) => this.handleProblemChange(event, i)}
                />
              </div>
              <div>
                <label>Answers</label>
              <div className="px-5 py-4">
                {e.answers.map((a,j) => (
                  <div key={j}>
                    <div className="form-group">
                      <label htmlFor={`acont_${i}_${j}`}>Answer no. {j+1}</label>
                      <div className="d-flex w-100 justify-content-between">
                        <input type="text" className="form-control" id={`acont_${i}_${j}`} placeholder={`Enter answer text`} name={`acont_${i}_${j}`} onChange={(evt) => {this.handleChangeOfAnswer(evt, i, j)}}/> 
                      
                        <div className="form-check">
                          <label className="ml-3 form-check-label" htmlFor={`answerCheckbox${i}_${j}`}>
                              <small>Correct answer?</small>
                          </label>
                          <input className="form-check-input" type="checkbox" value="" id={`answerCheckbox_${i}_${j}`} onClick={async () => { this.checkboxClicked(i,j)}}/>
                        </div>
                      </div>
                      
                      
                      
                    </div>
                  </div>
                ))} 
                <div className="d-flex w-100 align-items-center align-content-center justify-content-center">
                  <button className="my-3 btn btn-info btn-circle btn-md " onClick={(e) => this.addNewAnswer(e,i)}>+</button>
                </div>
                </div>
              </div>
            </div>
          ))}

          <div className="d-flex w-100 align-items-center align-content-center justify-content-center">
            <button className="my-4 btn btn-info btn-circle btn-md" onClick={(e) => this.addNewQuestion(e)} disabled={this.state.selected_domain == null || this.state.problems.length === this.state.test.questions.length}>+</button>
          </div>
        </div>
          <button type="submit" disabled={!this.everythingOkay()} className="btn btn-success col-8 offset-2 my-5">Add test</button>
        </form>
      </div>
      )}
      </div>
      
    );



  }
  
}

export default AddTest;
