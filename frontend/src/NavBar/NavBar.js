
import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import userService from '../Services/userService';

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refresh: 0
    };
  }

  async componentDidMount() {
    userService.onChange = (v) => this.setState({refresh: v});
  }

  render() {
    return (
      <nav className="navbar navbar-expand-lg sticky-top navbar-dark bg-primary">
        <Link className="navbar-brand" to="/">
          eKnowledge
        </Link>
  
        <button className="navbar-toggler border" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
  
        <div className="collapse navbar-collapse" id="navbarNavDropdown">
          <ul className="navbar-nav">
            
          {/* {localStorage.getItem('user-token') && localStorage.getItem('identity') && JSON.parse(localStorage.getItem('identity')).roles.includes('professor') && (
            <li className="nav-item">
              <Link className="nav-link px-4" to="/addTest">
                Add test
              </Link>
            </li>
          )} */}
            {localStorage.getItem('user-token') && localStorage.getItem('identity') && (
            <li className="nav-item">
              <Link className="nav-link px-4" to="/tests">
                Tests
              </Link>
            </li>
            )}
            {localStorage.getItem('user-token') && localStorage.getItem('identity') && JSON.parse(localStorage.getItem('identity')).roles.includes('professor') && (
            <li className="nav-item">
              <Link className="nav-link px-4" to="/domains">
                Domains
              </Link>
            </li>
          )}
            {!localStorage.getItem('user-token') && (
              <li className="nav-item">
                <Link className="nav-link px-4" to="/login">
                  Login
                </Link>
              </li>
            )}
            {!localStorage.getItem('user-token') && (
              <li className="nav-item">
                <Link className="nav-link px-4" to="/register">
                  Register
                </Link>
              </li>
            )}
            {localStorage.getItem('user-token') && (
              <li className="nav-item">
                <a href="/login" className="nav-link px-4" onClick={() => {userService.logout(); this.setState({refresh: this.state.refresh + 1})}}>
                  Logout
                </a>
              </li>
            )}
          
          </ul>
        </div>
      </nav>
    );
  }
  
}

export default NavBar;
