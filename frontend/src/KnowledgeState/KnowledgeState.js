


import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import testService from '../Services/testService';
import domainService from '../Services/domainService';
import { Redirect } from 'react-router-dom'
import ReadOnlyGraph from '../ReadOnlyGraph/ReadOnlyGraph'
import toast from "../Shared/Toast";

class KnowledgeState extends Component {
  constructor(props) {
    super(props);
    this.id = '';
    this.studentid = '';
    this.state = {
        test: null,
        domain: null,
        dataset: null,
        graph: null,
        markovian: null,
        message: '',
        message_mark: '',
    };
  }
   

    getKnowledgeStateGraph(graph) {
        
        let graph2 = {
            nodes: [],
            links: []
        }

        let currid = (() => {let cur = 1; return () => cur++})();
        const getAllSubsets = 
        theArray => theArray.reduce(
            (subsets, value) => subsets.concat(
                subsets.map(set => [value,...set])
            ),
            [[]]
        );

        let comb = getAllSubsets(graph.nodes.map(e => e.id));
        let final = JSON.parse(JSON.stringify(comb));
        comb.forEach((e,indeks) => {
            for (let i of e) {
                for (let d of graph.links.filter(x => x.target == i)) {
                    if (e.indexOf(d.source) == -1){
                        delete final[indeks]
                    }
                }
            }
        })
        final = final.filter(e => e).map(e => e.sort()).map(e => { return {id: currid(), values:e, name: e.map(x => graph.nodes.find(y => y.id == x).name).join(','), prids: e.map(x => graph.nodes.find(y => y.id == x).id)}});

        let finalinks = [];
        for (let i in final) {
            for (let j in final) {
                if (final[i].values.length + 1 !== final[j].values.length) continue;
                if (final[i].values.filter(x => final[j].values.indexOf(x) == -1).length > 0) continue;
                finalinks.push({source: final[i].id, target: final[j].id})
            }
        }

        graph2 = {nodes: final, links: finalinks}
        return graph2;
    }

  async componentDidMount() {
      debugger;
    if (!localStorage.getItem('identity'))
    {
      return;
    }
    
    let arraysEqual = (a,b) =>{
        if (a === b) return true;
        if (a == null || b == null) return false;
        if (a.length !== b.length) return false;
      
        a.sort();
        b.sort();
        for (var i = 0; i < a.length; ++i) {
          if (a[i] !== b[i]) return false;
        }
        return true;
      }

    let checksubset = (arr1, arr2) => arr2.every((el) => {
        return arr1.indexOf(el) !== -1;
    });
    const { match: { params } } = this.props;
    this.id = params.testID;
    this.studentid = params.studentid || JSON.parse(localStorage.getItem('identity')).id;

    let test = (await testService.getById(+this.id)).data;
    let domain = (await domainService.findById(test.domain_id)).data;
    
    let dataset = {
        nodes: domain.problems.map(e => { return { id: e.id, name: e.name } }),
        links: domain.relations.map(e => { return { source: e.child_id, target: e.parent_id } })
    }

    let graph = this.getKnowledgeStateGraph(dataset);
    let student_results =  (await testService.getStudentResults(this.id, this.studentid)).data;

    let allproblems = JSON.parse(JSON.stringify(domain.problems.map(e => e.id)));
    for (let question of test.questions) {
        let stud_results_filter = student_results.find(x => x.question_id == question.id);
        if (!stud_results_filter) allproblems.splice(allproblems.indexOf(question.problem_id), 1);
        let izbacio = false;
        for (let aq of stud_results_filter.aqid)
        {
            if (stud_results_filter.caid.indexOf(aq) === -1){
                izbacio = true;
                allproblems.splice(allproblems.indexOf(question.problem_id), 1);
                break;
            }
        }
        if (!izbacio && stud_results_filter.aqid.length !== stud_results_filter.caid.length) allproblems.splice(allproblems.indexOf(question.problem_id), 1);
    }



    let gn = JSON.parse(JSON.stringify(graph.nodes));
    let mgn = JSON.parse(JSON.stringify(graph.nodes));
    let studentstate = gn.find(e => arraysEqual(e.prids,allproblems));
    let altstate = null;
    if (!studentstate) {
        let allssstates = gn.filter(x => checksubset(allproblems, x.prids)).sort((a,b) => b.prids.length - a.prids.length);
        altstate = allssstates[0];
        altstate.fillColor = 'green';
    } else {
        studentstate.fillColor = 'green';
    }
    let message = studentstate?`Student is in state (${allproblems.map(e => domain.problems.find(x => x.id === e).name).join(',')}), and it is higlighted on chart`:`Student is in state (${allproblems.map(e => domain.problems.find(x => x.id === e).name).join(',')}), and it does not exist on chart. The most similar one (${altstate.prids.map(e => domain.problems.find(x => x.id === e).name).join(',')}) is highlighted on chart.`;
    graph.nodes = gn;

    let markovian = JSON.parse(JSON.stringify(graph));
    markovian.nodes.forEach(e => delete e.fillColor);
    let mallproblems = (await testService.getStudentState(test.id, this.studentid)).data;
    mallproblems = mallproblems[0].student_state.split(',').map(e => +e);
    let mstudentstate = mgn.find(e => arraysEqual(e.prids,mallproblems));
    mstudentstate.fillColor = 'green';
    markovian.nodes = mgn;
    let message_mark = `By Markovian probabilistic approach, student is in state (${mallproblems.map(e => domain.problems.find(x => x.id === e).name).join(',')}), and it is higlighted on chart`
    this.setState({test,domain,dataset,graph,markovian, message, message_mark});
  }

  

  render() {
    if (!localStorage.getItem('identity'))
    {
      return <Redirect to='/unauthorized' />
    }
    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-3 text-center offset-05">
                    <h3 className="my-4">Knowledge graph</h3>
                    <h5 className="my-4 text-light"> .</h5>
                    {this.state.dataset && (<ReadOnlyGraph chartid={'chart1'} dataset={this.state.dataset} width={window.innerWidth * 3/12} height={500}></ReadOnlyGraph>)}
                </div>
                <div className="col-3 offset-1">
                    <h3 className="my-4">Knowledge state</h3>
                    <h5 className="my-4"> (Naive)</h5>
                    {this.state.graph && (<ReadOnlyGraph chartid={'chart2'} dataset={this.state.graph} width={window.innerWidth * 3/12} height={500}></ReadOnlyGraph>)}
                    
                </div>
                
                <div className="col-3 offset-1">
                    <h3 className="my-4">Knowledge state</h3>
                    <h5 className="my-4"> (Stohastic Markovian)</h5>
                    {this.state.graph && (<ReadOnlyGraph chartid={'chart3'} dataset={this.state.markovian} width={window.innerWidth * 3/12} height={500}></ReadOnlyGraph>)}
                    
                </div>
                <div className="col-3 offset-45 mt-5">
                    <h5>{this.state.message}</h5>
                </div>
                <div className="col-3 offset-1 mt-5">
                    <h5>{this.state.message_mark}</h5>
                </div>
            </div>
        </div>
    );
  }
  
}

export default KnowledgeState;
