import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import ReadOnlyGraph from '../ReadOnlyGraph/ReadOnlyGraph.js'

class Welcome extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  async componentDidMount() {
    this.setState({dataset: {
      nodes: [
          {id: 0, name: "Sabiranje do 10"},
          {id: 1, name: "Sabiranje do 1000"},
          {id: 2, name: "Sabiranje do 100000"},
          {id: 3, name: "Osnove množenja brojeva"},
          {id: 4, name: "Množenje brojeva do 10"},
          {id: 5, name: "Množenje brojeva do 1000"},
          {id: 6, name: "Osnove algebre"},
          {id: 7, name: "Osnove matrica"},
          {id: 8, name: "Osnove programiranja"},
          {id: 9, name: "Algoritmi i strukture podataka"},
          {id: 10, name: "Osnove autizma"},
          {id: 11, name: "Pun mi je kurac ovog projekta"},
          {id: 12, name: "Strašno mi je loše"},
          {id: 13, name: "Osnove veštačke inteligencije"},
          {id: 14, name: "Osnove veštačke inteligencije222222222"},
      ],
      links: [
          {source: 0, target: 1},
          {source: 1, target: 2},
          {source: 1, target: 4},
          {source: 3, target: 4},
          {source: 4, target: 5},
          {source: 5, target: 6},
          {source: 5, target: 7},
          {source: 6, target: 8},
          {source: 8, target: 9},
          {source: 7, target: 11},
          {source: 11, target: 12},
          {source: 9, target: 13},
          {source: 10, target: 13},
          {source: 12, target: 13},
      ]
  }})
  }

  render() {
    return (
      <div id="page-top">
        <header className="masthead">
            <div className="container h-100">
                <div className="row h-100 align-items-center justify-content-center text-center">
                    <div className="col-lg-10 align-self-end">
                        <h1 className="text-uppercase text-white font-weight-bold">willkommen to ze eknowledge</h1>
                        <hr className="divider my-4" />
                    </div>
                    <div className="col-lg-8 align-self-baseline">
                        <p className="text-white-75 font-weight-light mb-5">eKnowledge can help you do tests and visualize some nasty results. Very well.</p>
                        <Link to="/login" className="btn btn-primary btn-xl js-scroll-trigger">Find Out More</Link>
                    </div>
                </div>
            </div>
        </header>
        <section className="page-section bg-primary" id="about">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-lg-8 text-center">
                        <h2 className="text-white mt-0">We've got what you need!</h2>
                        <hr className="divider light my-4" />
                        <p className="text-white-50 mb-4">eKnowledge has everything you need to successfully do some tests. It is the best out there, among other eKnowledges, most definitely worse than this one.</p>
                        <Link to="/login" className="btn btn-light btn-xl js-scroll-trigger">Get Started!</Link>
                    </div>
                </div>
            </div>
        </section>
        <footer className="bg-light py-5">
            <div className="container"><div className="small text-center text-muted">Copyright © 2020 - Fakultet tehničkih nauka</div></div>
        </footer>
        

      </div>
      
    );
  }
  
}

export default Welcome;
