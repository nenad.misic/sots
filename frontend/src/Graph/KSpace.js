import { Component } from 'react';
import testService from "../Services/testService";
import ksService from "../Services/ksService";
import domainService from "../Services/domainService";
import problemService from "../Services/problemService";
import ReadOnlyGraph from '../ReadOnlyGraph/ReadOnlyGraph'
import toast from "../Shared/Toast";
import GraphDifference from '../GraphDifference/GraphDifference';

class KSpace extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dataset: null,            
            prdataset: null
        }
    }

    async componentDidMount() {
        let kspace = (await ksService.findById(+this.props.match.params.testID)).data;
        let dataset = {
            nodes: kspace.problems.map(e => { return { id: e.id, name: e.name } }),
            links: kspace.relations.map(e => { return { source: e.child_id, target: e.parent_id } })
        }
        
        let qid = (await testService.getById(+this.props.match.params.testID)).data.questions[0].problem_id;
        let domainid = (await problemService.findById(qid)).data.domain_id

        let data = (await domainService.findById(domainid)).data;
        let prdataset = {
            nodes: data.problems.map(e => {return  { id: e.id, name: e.name}}),
            links: data.relations.map(e => { return { _id: e.id, source: e.child_id, target: e.parent_id}})
        }

        this.setState({ prdataset, dataset });
    }

    saveKSpace() {
        let relations = this.state.dataset.links.map(l => [l.source.id, l.target.id]);
        let data = {
            test_id: +this.props.match.params.testID,
            relations
        }

        ksService.create(data).then(toast.success('Real knowledge space has successfully saved')).catch(reason => toast.error(reason));
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-3 ml-5 text-center">
                        <h3 className="my-4">Knowledge graph according to Inductive Item Tree Analysis</h3>
                        {this.state.dataset && (<ReadOnlyGraph chartid={'chart1'} dataset={this.state.dataset} width={window.innerWidth * 3/12} height={500}></ReadOnlyGraph>)}
                        <button className="btn btn-success mt-4" onClick={() => this.saveKSpace()} disabled={this.state.dataset && this.state.dataset.links.length === 0}> Save </button>
                    </div>
                    <div className="col-3 offset-1 text-center">
                        <h3 className="my-4">Knowledge graph created by professor</h3>
                        {this.state.prdataset && (<ReadOnlyGraph chartid={'chart2'} dataset={this.state.prdataset} width={window.innerWidth * 3/12} height={500}></ReadOnlyGraph>)}
                        
                    </div>
                    
                    <div className="col-3 offset-1 text-center">
                        <h3 className="my-4">Graph difference visualization</h3>
                        {this.state.prdataset && (<GraphDifference chartid={'chart3'} links1={this.state.prdataset.links} links2={this.state.dataset.links} nodes={this.state.prdataset.nodes} width={window.innerWidth * 3/12} height={500}></GraphDifference>)}
                    
                        <h6 className="my-4">Green - present in both knowledge graphs</h6>
                        <h6 className="my-4">Red - present only in IITA knowledge space</h6>
                        <h6 className="my-4">Blue - present only in expected knowledge space</h6>
                    </div>
                </div>
                
            </div>
        );
    }
}

export default KSpace;