import React, {Component} from 'react';
import domainService from '../Services/domainService';
import problemService from '../Services/problemService';
import relationService from '../Services/relationService';
import * as d3 from "d3";

class Graph extends Component {

    constructor(props) {
        super(props);
        this.deleteZeRelationship = ()=>{};
        this.deleteZeNode = ()=>{};
        this.addZeNode = ()=>{};
        this.domainId = -1;
        this.state = {
            dataset: null
        };
    }
    async componentDidMount() {
        
        const { match: { params } } = this.props;
        this.domainId = params.domainId;
        // let dataset = {
        //     nodes: [
        //         {id: 0, name: "Sabiranje do 10"},
        //         {id: 1, name: "Sabiranje do 1000"},
        //         {id: 2, name: "Sabiranje do 100000"},
        //         {id: 3, name: "Osnove množenja brojeva"},
        //         {id: 4, name: "Množenje brojeva do 10"},
        //         {id: 5, name: "Množenje brojeva do 1000"},
        //         {id: 6, name: "Osnove algebre"},
        //         {id: 7, name: "Osnove matrica"},
        //         {id: 8, name: "Osnove programiranja"},
        //         {id: 9, name: "Algoritmi i strukture podataka"},
        //         {id: 10, name: "Osnove autizma"},
        //         {id: 11, name: "Pun mi je kurac ovog projekta"},
        //         {id: 12, name: "Strašno mi je loše"},
        //         {id: 13, name: "Osnove veštačke inteligencije"},
        //         {id: 14, name: "Osnove veštačke inteligencije222222222"},
        //     ],
        //     links: [
        //         {source: 0, target: 1},
        //         {source: 1, target: 2},
        //         {source: 1, target: 4},
        //         {source: 3, target: 4},
        //         {source: 4, target: 5},
        //         {source: 5, target: 6},
        //         {source: 5, target: 7},
        //         {source: 6, target: 8},
        //         {source: 8, target: 9},
        //         {source: 7, target: 11},
        //         {source: 11, target: 12},
        //         {source: 9, target: 13},
        //         {source: 10, target: 13},
        //         {source: 12, target: 13},
        //     ]
        // }
        
        let data = (await domainService.findById(this.domainId)).data;
        let dataset = {
            nodes: data.problems.map(e => {return  { id: e.id, name: e.name}}),
            links: data.relations.map(e => { return { _id: e.id, source: e.child_id, target: e.parent_id}})
        }
        this.setState({dataset});
        [this.deleteZeRelationship, this.deleteZeNode, this.addZeNode] = this.drawChart(this,dataset);
    }

    cycleHelper (node, originalId, rel) {
        if (node.id === originalId) {
            return true;
        }
        if (!originalId) {
            originalId = node.id;
        }
        return rel.filter(x=>x.source == node.id).map(x=> this.cycleHelper(this.state.dataset.nodes.find(y => y.id == x.target), originalId, rel)).filter(e => e==true).length > 0;
    }
    
    cyclicGraph (src, dst) {
        let rel = this.state.dataset.links.map(e => {
            return {
                source: typeof(e.source) == typeof(1)?e.source:e.source.id,
                target: typeof(e.target) == typeof(1)?e.target:e.target.id
            }
        });
        rel.push({source: src.id, target: dst.id})
        for (let node of this.state.dataset.nodes) {
          if (this.cycleHelper(node, null, rel)) return true;
        }
        return false;
    
    }

    drawChart(that,ds) {
        var dataset = ds;

        
        let margin = ({top: 30, right: 80, bottom: 30, left: 30}); 
        let width = window.innerWidth*7/12 - margin.left - margin.right;
        let height = window.innerHeight*.8;

        
        var div = d3.select("body").append("div")	
            .attr("class", "tooltip")				
            .style("opacity", 0);


        
        
        
        function rerenderer() {
            var timeoutfun = null;
            var current_hovered = null;
            var current_add_relationship = null;
            d3.select('#chart').html('');
            
            let simulation = d3.forceSimulation()
                .force("link", d3.forceLink()
                                .id(d => d.id).distance(100)
                ) 
                .force("charge", d3.forceManyBody().strength(-250))
                .force("center", d3.forceCenter().x(width * .5).y(height * .5));

            const cont = d3.select('#chart')
                .append("svg")
            
            cont
                .append('defs').append('marker')
                .attr("id",'arrowhead')
                .attr('viewBox','-0 -5 10 10') //the bound of the SVG viewport for the current SVG fragment. defines a coordinate system 10 wide and 10 high starting on (0,-5)
                .attr('refX',23) // x coordinate for the reference point of the marker. If circle is bigger, this need to be bigger.
                .attr('refY',0)
                .attr('orient','auto')
                    .attr('markerWidth',10)
                    .attr('markerHeight',10)
                    .attr('xoverflow','visible')
                .append('svg:path')
                .attr('d', 'M 0,-5 L 10 ,0 L 0,5')
                .attr('fill', '#999')
                .style('stroke','none');
            const svg = cont
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .call(d3.zoom().on("zoom", function (d) {
                    svg.attr("transform", d3.event.transform)
                }))
            .append("g")
                .attr("transform", `translate(${margin.left},${margin.top})`);

            svg.append('rect')
                .attr('x', -margin.left)
                .attr('y', -margin.top)
                .attr('width', width + margin.left + margin.right)
                .attr('height', height + margin.top + margin.bottom)
                .attr('fill', 'white')
                .on('mouseover', function() {
                    addButton.attr('visibility', 'hidden');
                    addButtonText.attr('visibility', 'hidden');
                    addNodeButton.attr('visibility', 'hidden');
                    addNodeButtonText.attr('visibility', 'hidden');
                });


            const link = svg.append("g")
                .attr("class", "links")
                .selectAll("line")
                .data(dataset.links)
            .enter().append("line")
            .attr('marker-end','url(#arrowhead)')
            .on('mouseover', function() {
                console.log('heeej');
                d3.select(this).style('fill', 'red');
            }); ;

            const node = svg.append("g")
                .attr("class", "nodes_invis")
                .selectAll("circle")
                .data(dataset.nodes)
            .enter().append("circle")
                .attr("class", "invisible_circle")
                .attr("r", 40)
                .on("mouseover", function(d) {
                    current_hovered = d;
                    makeAddButtonVisible(d3.select(this), that);
                    makeAddNodeButtonVisible(d3.select(this));
                })
                
            const node2 = svg.append("g")
                .attr("class", "nodes")
                .selectAll("circle")
                .data(dataset.nodes)
            .enter().append("circle")
                .attr("class", "colored_circle")
                .attr("r", 15)
                .attr('id', d => `circle${d.id}`)
                .on("mouseover", function(d) {
                    current_hovered = d;
                    makeAddButtonVisible(d3.select(this), that);
                    makeAddNodeButtonVisible(d3.select(this));
                })
                .call(d3.drag()
                    .on("start", dragstarted)
                    .on("drag", dragged)
                    .on("end", dragended)
                );

            const addNodeButton = svg.append('g')
                .attr('class', 'add_node')
                .append('circle')
                .attr('r', 10)
                .attr('class','add_node_btn')
                .style('fill', '#f0ad4e')
                .attr('visibility', 'hidden')
                .on("mouseover", function() {
                    d3.select(this).style('fill', '#ec971f');
                    div.transition()		
                    .duration(200)		
                    .style("opacity", .9);		
                    div.html('Add new node as child of this one')	
                    .style("left", (+d3.event.pageX + 30) + "px")		
                    .style("top", (d3.event.pageY - 28) + "px");
                    timeoutfun = setTimeout(() => {
                        div.transition()		
                        .duration(500)		
                        .style("opacity", 0);
                    }, 2000)	
                })
                .on("mouseout", function(){
                    d3.select(this).style('fill', '#f0ad4e')
                    clearTimeout(timeoutfun);
                    div.transition()		
                        .duration(500)		
                        .style("opacity", 0);	
                })
                .on('click', async () => {
                    let addedNode = await addNode({name: document.getElementById('nodenameinput').value});
                    if (addedNode.okay)
                    {
                        await addRelation(addedNode.id, current_hovered.id);
                        rerenderer();
                    }
                });

            
            const addNodeButtonText = svg.append('g')
                .attr('class', 'add_node_text')
                .append('text')
                .text("+")
                .style("font-size", "24px")
                .style("text-anchor", "middle")
                .style("alignment-baseline", "central")
                .attr('visibility', 'hidden');

            
            const addButton = svg.append('g')
                .attr('class', 'add_node')
                .append('circle')
                .attr('r', 10)
                .attr('class','add_node_btn')
                .style('fill', '#4bbf73')
                .attr('visibility', 'hidden')
                .on("mouseover", function() {
                    d3.select(this).style('fill', '#3ca861');
                    div.transition()		
                    .duration(200)		
                    .style("opacity", .9);		
                    div.html('Add new relation')	
                    .style("left", (+d3.event.pageX + 30) + "px")		
                    .style("top", (d3.event.pageY - 28) + "px");	
                    timeoutfun = setTimeout(() => {
                        div.transition()		
                        .duration(500)		
                        .style("opacity", 0);
                    }, 2000)	
                })
                .on("mouseout", function(){
                    d3.select(this).style('fill', '#4bbf73')
                    clearTimeout(timeoutfun);
                    div.transition()		
                        .duration(500)		
                        .style("opacity", 0);	
                })
                .on('click', async () => {
                    if(current_add_relationship === null){
                        current_add_relationship = current_hovered;
                        d3.select(`#circle${current_hovered.id}`).style('fill', '#3ca861');
                    }
                    else {
                        let addedRelation = (await addRelation(current_add_relationship.id, current_hovered.id));
                        if (addedRelation.okay) {
                            current_add_relationship = null;
                            rerenderer();
                        }

                    }
                });

            
            const addButtonText = svg.append('g')
                .attr('class', 'add_node_text')
                .append('text')
                .text("+")
                .style("font-size", "24px")
                .style("text-anchor", "middle")
                .style("alignment-baseline", "central")
                .attr('visibility', 'hidden');

            
            // Text to nodes
            const text = svg.append("g")
                .attr("class", "text")
                .selectAll("text")
                .data(dataset.nodes)
            .enter().append("text")
                .text(d => d.name)
                .attr('dx', 25)

            simulation
                .nodes(dataset.nodes)
                .on("tick", ticked);

            simulation.force("link")
                .links(dataset.links);

            function ticked() {
                link.attr("x1", d => {
                    return d.source.x
                })
                .attr("y1", d => {
                    return d.source.y
                })
                .attr("x2", d => {
                    return d.target.x
                })
                .attr("y2", d => {
                    return d.target.y
                });
    
                node.attr("cx", d => {
                    return d.x;
                })
                    .attr("cy", d => { 
                        return d.y;
                });
    
                node2.attr("cx", d => d.x)
                    .attr("cy", d => d.y);
    
                text.attr("x", d => d.x - 5)
                    .attr("y", d => d.y + 5);
            }
    
            function dragstarted(d) {
                if (!d3.event.active) simulation.alphaTarget(0.3).restart();
                addButton.attr('visibility', 'hidden');
                addButtonText.attr('visibility', 'hidden');
                addNodeButton.attr('visibility', 'hidden');
                addNodeButtonText.attr('visibility', 'hidden');
                d.fy = d.y;
                d.fx = d.x;
            }
    
            function dragged(d) {
                d.fx = d3.event.x;
                d.fy = d3.event.y;
            }
    
            function dragended(d) {
                // addButton.attr('visibility', 'visible');
                // addButtonText.attr('visibility', 'visible');
                if (!d3.event.active) simulation.alphaTarget(0);
                d.fx = null;
                d.fy = null;
            }

            function makeAddNodeButtonVisible(that) {
                if ( document.getElementById('nodenameinput').value !== '') {
                    addNodeButton.attr('visibility', 'visible');
                    addNodeButtonText.attr('visibility', 'visible');
                    addNodeButton.attr('cx', +that.attr('cx') + 25);
                    addNodeButton.attr('cy',  +that.attr('cy') + 25); 
                    addNodeButtonText.attr('x', +that.attr('cx') + 25);
                    addNodeButtonText.attr('y',  +that.attr('cy') + 25); 
                }
            }
    
            function makeAddButtonVisible(that, upperthat) {
                if (current_add_relationship !== null) {
                    if (dataset.links.filter(x => x.source.id === current_add_relationship.id && x.target.id === current_hovered.id).length === 0 &&
                        dataset.links.filter(x => x.target.id === current_add_relationship.id && x.source.id === current_hovered.id).length === 0 &&
                        current_add_relationship.id !== current_hovered.id && !upperthat.cyclicGraph(current_add_relationship, current_hovered))  {
                        addButton.attr('visibility', 'visible');
                        addButtonText.attr('visibility', 'visible');
                        addButton.attr('cx', +that.attr('cx') + 25);
                        addButton.attr('cy',  +that.attr('cy') - 25); 
                        addButtonText.attr('x', +that.attr('cx') + 25);
                        addButtonText.attr('y',  +that.attr('cy') - 25); 
                    }
                } else {
                    addButton.attr('visibility', 'visible');
                    addButtonText.attr('visibility', 'visible');
                    addButton.attr('cx', +that.attr('cx') + 25);
                    addButton.attr('cy',  +that.attr('cy') - 25); 
                    addButtonText.attr('x', +that.attr('cx') + 25);
                    addButtonText.attr('y',  +that.attr('cy') - 25); 
                }
            }

            async function addNode(node) {
                if ( document.getElementById('nodenameinput').value !== '') {
                    let nodeid = await problemService.create({name: node.name, domain_id: that.domainId});
                    node.id = nodeid.data.id;
                    dataset.nodes.push(node);

                    return {okay: true, id: nodeid.data.id};
                }
                return {okay: false, id: -1};
            }
    
            async function addRelation(source, target) {
                debugger;
                console.log('aaaaaaaaaaaaaaaaaaaa');
                if (dataset.links.filter(x => x.source.id === source && x.target.id === target.id).length === 0 &&
                            dataset.links.filter(x => x.target.id === source && x.source.id === target.id).length === 0 &&
                            source !== target.id)  {
                                dataset.links = dataset.links.map( e => {
                                    return {
                                        source: e.source,
                                        target: e.target
                                    }
                                })
                                let relationid = await relationService.create({parent_id: target, child_id: source});
                                dataset.links.push({_id: relationid.data.id, source, target});
                                return {okay: true, id: relationid.data.id};
                }
                return {okay: false, id: -1};
                
                
            }

            async function deleteRelationship(id) {
                relationService.delete(id).then(async () => {
                    let data = (await domainService.findById(that.domainId)).data;
                    dataset.links = data.relations.map(e => { return { _id: e.id, source: e.child_id, target: e.parent_id}})
                    rerenderer();
                }).catch(err => alert('error'));
                // TODO toast
                
            }
            async function deleteNode(id) {
                let relationsToDelete = dataset.links.map( e => {
                    return {
                        _id: e._id,
                        source: e.source,
                        target: e.target
                    }
                }).filter(x => x.source.id === id || x.target.id === id).map( y => y._id);

                let deletePromiseList = [];
                for (let i of relationsToDelete) {
                    deletePromiseList.push(relationService.delete(i));
                }
                Promise.all(deletePromiseList).then(() => {
                    problemService.delete(id).then(async () => {
                        let data = (await domainService.findById(that.domainId)).data;
                        dataset.nodes = data.problems.map(e => {return  { id: e.id, name: e.name}});
                        dataset.links = data.relations.map(e => { return { _id: e.id, source: e.child_id, target: e.parent_id}});
                        rerenderer();
                    }).catch(err => alert('err'));
                    // TODO toast
                }).catch(err => alert('err'));
                // TODO toast
            }
            async function addNodeWithoutRelation() {
                await addNode({name: document.getElementById('nodenameinput').value});
                rerenderer();
            }

            that.setState({dataset});

            return [deleteRelationship, deleteNode, addNodeWithoutRelation];

        }

        

        return rerenderer();
    }

    
    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    
                    <div id="chart" className="col-7">

                    </div>
                    <div id="rest" className="col-35 offset-05">

                    <div className="form-group">
                        <div className="row">
                            <input type="text" className="form-control col-8 mt-1" id="nodenameinput" placeholder="Enter node name"/> 
                            <div className="btn btn-outline-success offset-1 col-3" onClick={() => this.addZeNode()}>Add node</div>
                        </div>
                    </div>

                    <hr></hr>
                    <h3>Nodes</h3>

                        <div className="scrollablepanel px-4">
                        {this.state.dataset && this.state.dataset.nodes.map(e => (
                            <div key={e.id} className="row my-2">
                                <span className="border border-primary col-9 d-flex justify-content-center align-items-center">
                                    {e.name}
                                </span>
                                <span className="btn btn-danger col-3" onClick={() => this.deleteZeNode(e.id)}>
                                    Delete
                                </span>
                            </div>
                                
                        ))}
                        </div>

                        <hr></hr>
                    <h3>Relations</h3>

                    <div className="scrollablepanel px-4">
                    {this.state.dataset && this.state.dataset.links.map(e => (
                        <div key={e._id} className="row my-2">
                            <span className="border border-primary col-9 d-flex justify-content-center align-items-center">
                                {e.source.name} - {e.target.name}
                            </span>
                            <span className="btn btn-danger col-3" onClick={() => this.deleteZeRelationship(e._id)}>
                                Delete
                            </span>
                        </div>
                            
                    ))}
                    </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default Graph;