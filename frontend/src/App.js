import React, { Component } from 'react';
import {Route} from 'react-router-dom';
import NavBar from './NavBar/NavBar';
import Login from './Login/Login';
import Register from './Register/Register';
import Welcome from './Welcome/Welcome';
import Unauthorized from './Unauthorized/Unauthorized';
import TestList from './Tests/TestList';
import SolveTest from './Tests/SolveTest';
import SolveTestGuided from './Tests/SolveTestGuided';
import TestResults from './Tests/TestResults';
import AddTest from './Tests/AddTest';
import DomainList from './Domains/DomainList';
import AddDomain from './Domains/AddDomain';
import KSpace from './Graph/KSpace';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Graph from './Graph/Graph';
import ReadOnlyGraph from './ReadOnlyGraph/ReadOnlyGraph';
import KnowledgeState from './KnowledgeState/KnowledgeState';

import StudentResults from './StudentResults/StudentResults';




class App extends Component {

  render() {
    return (
      <div className="App">
        <NavBar></NavBar>
        <Route exact path='/' component={Welcome}/>
        <Route exact path='/unauthorized' component={Unauthorized}/>
        <Route exact path='/login' component={Login}/>
        <Route exact path='/register' component={Register}/>
        <Route exact path='/tests' component={TestList}/>
        <Route exact path='/test/:testID' component={SolveTestGuided}/>
        <Route exact path='/test/:testID/kspace' component={KSpace}/>
        <Route exact path='/test/:testID/results' component={StudentResults}/>
        <Route exact path='/addTest' component={AddTest}/>
        <Route exact path='/graph/:domainId' component={Graph}/>
        <Route exact path='/rograph' component={ReadOnlyGraph}/>
        <Route exact path='/domains' component={DomainList}/>
        <Route exact path='/addDomain' component={AddDomain}/>
        <Route exact path='/kstate/:testID/:studentid' component={KnowledgeState}/>
        <Route exact path='/testresults/:testID/:studentid' component={TestResults}/>
        {/* Primeri ruta */}
        {/* <Route exact path='/' component={NekaKomponenta}/>
        <Route exact path='/stagod' component={NekaKomponenta}/>
        <Route exact path='/stagod2/:param_id_recimo' component={NekaKomponenta}/> */}
        <ToastContainer position="bottom-center" autoClose={5000} hideProgressBar newestOnTop closeOnClick rtl={false} pauseOnFocusLoss draggable pauseOnHover />
      </div>
    );
  }
}

export default App;
