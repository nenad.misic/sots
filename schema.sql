-- Mostly USER tables and their relations:

CREATE TABLE user (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	first_name TEXT NOT NULL,
	last_name TEXT NOT NULL,
	username TEXT NOT NULL UNIQUE,
	password TEXT NOT NULL 
);

CREATE TABLE role (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	role_name TEXT NOT NULL
);

CREATE TABLE user_role (
	u_id INTEGER,
	r_id INTEGER,
    PRIMARY KEY (u_id, r_id),
    FOREIGN KEY (u_id) 
    REFERENCES user (id) 
    ON DELETE NO ACTION 
    ON UPDATE NO ACTION,
    FOREIGN KEY (r_id) 
    REFERENCES role (id) 
    ON DELETE NO ACTION 
    ON UPDATE NO ACTION
);

-- Mostly TEST, QUESTION and ANSWER tables and their relations:

CREATE TABLE test (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	test_name TEXT NOT NULL,
	domain_id INTEGER,
    description TEXT,
    date DATE NOT NULL,
    FOREIGN KEY (domain_id) 
    REFERENCES domain (id) 
    ON DELETE NO ACTION 
    ON UPDATE NO ACTION
);

CREATE TABLE question (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	content TEXT NOT NULL,
	test_id INTEGER,
    points INTEGER NOT NULL,
    problem_id INTEGER, -- TODO Add NOT NULL constraint
    FOREIGN KEY (test_id) 
    REFERENCES test (id) 
    ON DELETE CASCADE 
    ON UPDATE NO ACTION,
    FOREIGN KEY (problem_id)
    REFERENCES problem (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TABLE answer (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	content TEXT NOT NULL,
    question_id INTEGER,
    FOREIGN KEY (question_id) 
    REFERENCES question (id) 
    ON DELETE CASCADE 
    ON UPDATE NO ACTION
);

CREATE TABLE correct_answer (
    answer_id INTEGER,
    question_id INTEGER,
    PRIMARY KEY (answer_id, question_id),
    FOREIGN KEY (answer_id) 
    REFERENCES answer (id) 
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
    FOREIGN KEY (question_id) 
    REFERENCES question (id) 
    ON DELETE CASCADE
    ON UPDATE NO ACTION
);

CREATE TABLE professor_of (
    u_id INTEGER,
    test_id INTEGER,
    PRIMARY KEY (u_id, test_id),
    FOREIGN KEY (test_id) 
    REFERENCES test (id) 
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
    FOREIGN KEY (u_id) 
    REFERENCES user (id) 
    ON DELETE CASCADE
    ON UPDATE NO ACTION
);

CREATE TABLE student_of
(
    u_id INTEGER,
    test_id INTEGER,
    solved BOOLEAN,
    points INTEGER,
    student_state TEXT DEFAULT '',
    PRIMARY KEY (u_id, test_id),
    FOREIGN KEY (test_id) 
    REFERENCES test (id) 
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
    FOREIGN KEY (u_id) 
    REFERENCES user (id) 
    ON DELETE CASCADE
    ON UPDATE NO ACTION
);

CREATE TABLE answered_question
(
    u_id INTEGER,
    answer_id INTEGER,
    PRIMARY KEY (u_id, answer_id),
    FOREIGN KEY (answer_id) 
    REFERENCES answer (id) 
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
    FOREIGN KEY (u_id) 
    REFERENCES user (id) 
    ON DELETE CASCADE
    ON UPDATE NO ACTION
);

-- Mostly DOMAIN and PROBLEM tables and their relations:

CREATE TABLE domain
(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL,
    description TEXT NOT NULL
);

CREATE TABLE problem
(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL,
    domain_id INTEGER NOT NULL,
    FOREIGN KEY (domain_id)
    REFERENCES domain (id)
    ON DELETE CASCADE
    ON UPDATE NO ACTION
);

CREATE TABLE problem_relation
(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    parent_id INTEGER,
    child_id INTEGER,
    UNIQUE (parent_id, child_id),
    FOREIGN KEY (parent_id) 
    REFERENCES problem (id) 
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
    FOREIGN KEY (child_id) 
    REFERENCES problem (id) 
    ON DELETE CASCADE
    ON UPDATE NO ACTION
);

-- Mostly RELATION table of knowledge spaces:

CREATE TABLE expec_ks
(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    test_id INTEGER UNIQUE,
    FOREIGN KEY (test_id)
    REFERENCES test (id)
    ON DELETE CASCADE
    ON UPDATE NO ACTION
);

CREATE TABLE ks_relation
(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    expec_ks_id INTEGER,
    parent_id INTEGER,
    child_id INTEGER,
    UNIQUE (parent_id, child_id),
    FOREIGN KEY (expec_ks_id)
    REFERENCES expec_ks (id)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
    FOREIGN KEY (parent_id) 
    REFERENCES problem (id) 
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
    FOREIGN KEY (child_id) 
    REFERENCES problem (id) 
    ON DELETE CASCADE
    ON UPDATE NO ACTION
);
