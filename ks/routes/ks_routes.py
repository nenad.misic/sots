from flask import Blueprint
from services import ks_service

ks_api = Blueprint('ks_api', __name__)


@ks_api.route('/kspace/<int:test_id>')
def construct_kspace(test_id):
    return ks_service.construct_kspace(test_id)


@ks_api.route('/kstate/<int:student_id>')
def construct_kstate(student_id):
    return ks_service.construct_kstate(student_id)
