import pandas as pd
from repositories import ks_repository
from alg import iita


def construct_kspace(test_id):
    ques_ca, ans_ques, ques_prob = ks_repository.get_test_results(test_id)

    # Dict with question and corresponding correct answers
    ques_ca_dict = dict((x, []) for x, y in ques_ca)
    for ques in ques_ca:
        ques_ca_dict.get(ques[0]).append(ques[1])

    # Dict with users with answers for corresponding questions
    ans_ques_dict = dict((x, dict()) for x, y, z in ans_ques)
    for user in ans_ques:
        ans_ques_dict[user[0]] = dict((y, []) for x, y, z in ans_ques if x == user[0])

    for user in ans_ques:
        ans_ques_dict[user[0]][user[1]].append(user[2])

    user_results_dict = dict()
    for user in ans_ques_dict:
        user_results_dict[user] = []

        for ques in ans_ques_dict[user]:
            if ans_ques_dict[user][ques] == ques_ca_dict[ques]:
                user_results_dict[user].append(1)
            else:
                user_results_dict[user].append(0)

    user_results = [user_results_dict[user] for user in user_results_dict]
    transpose_user_results = [[user_results[j][i] for j in range(len(user_results))] for i in
                              range(len(user_results[0]))]

    result_dict = dict((y, []) for x, y in ques_prob)

    i = 0
    for prob in result_dict:
        result_dict[prob] = transpose_user_results[i]
        i += 1

    keys_list = list(result_dict)

    # print(user_results_dict)
    # print(result_dict)

    data_frame = pd.DataFrame(user_results_dict)
    implications = iita(data_frame.to_numpy(), 1)['implications']
    result = {'implications': [[keys_list[x] for x in y] for y in implications]}

    return result


def construct_kstate(student_id):
    return student_id
