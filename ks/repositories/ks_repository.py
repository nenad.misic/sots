import sqlite3
from sqlite3 import Error


def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn


def get_test_results(test_id):
    with create_connection('..\\database.db') as conn:
        cur = conn.cursor()

        cur.execute('SELECT question.id AS question_id, correct_answer.answer_id AS answer_id FROM question INNER '
                    'JOIN correct_answer ON question.id = correct_answer.question_id WHERE question.test_id = ? '
                    'ORDER BY question_id, answer_id;', [test_id])
        ques_ca = cur.fetchall()

        cur.execute('SELECT answered_question.u_id AS user_id, question.id AS question_id, '
                    'answered_question.answer_id AS answer_id FROM question INNER JOIN answered_question ON '
                    'question.id = answer.question_id INNER JOIN answer ON answer.id = answered_question.answer_id '
                    'WHERE question.test_id = ? ORDER BY user_id, question_id, answer_id;', [test_id])
        ans_ques = cur.fetchall()

        cur.execute('SELECT question.id AS question_id, problem.id AS problem_id FROM question INNER JOIN problem ON '
                    'question.problem_id = problem.id WHERE question.test_id = ? ORDER BY question_id;', [test_id])
        ques_prob = cur.fetchall()

        return ques_ca, ans_ques, ques_prob
