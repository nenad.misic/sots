from flask import Flask
from routes.ks_routes import ks_api

app = Flask(__name__)
app.register_blueprint(ks_api)


@app.route('/')
def hello_world():
    return 'Hello!'


if __name__ == '__main__':
    app.run()
