
# Platforma za testiranje znanja

U nastavku je dat kratak opis rada i funkcionalnosti platforme za testiranje znanja učenika, koja je implementirana kao projektni zadatak iz predmeta "Savremene obrazovne tehnologije i standardi" u okviru master akademskih studija na Fakultetu tehničkih nauka, Novi Sad.

## Platforma

Platforma omogućava testiranje znanja učenika kroz testove na efikasan način primenom vođenog testiranja.
Korisnici ove platforme su nastavnici i učenici.

### Nastavnik

Nastavnik može da:
- definiše očekivani prostor znanja (domen, u formi grafa)
- definiše testove
- vidi kako izgleda realni prostor znanja za određeni domen (u formi grafa)
- vidi rezultate testa svakog učenika i njegovo stanje znanja (u formi grafa)

Test se sastoji iz pitanja. Svako pitanje se odnosi na jedan problem iz domena i može da ima jedan ili više tačnih odgovora. Test je moguće eksportovati u [IMS QTI format](https://www.imsglobal.org/spec/qti/v3p0/guide).

Realni prostor znanja za neki domen dobija se primenom *Inductive Item Tree Analysis*<sup>1</sup> metode na odgovore svih učenika za taj domen. Implementacije metode nalaze se na putanji *ks/alg* i u svrhe projekta korišćena je *Minimized Corrected Inductive Item Tree Analysis* (*mini_iita.py*).

Stanje znanja učenika identifikuje se korišćenjem *Stohastičke Markovljeve procedure*<sup>2</sup> i prikazuje se na grafu.

<hr/>
<sup>1</sup> Schrepp, M. (2006). "ITA 2.0: A program for Classical and Inductive Item Tree Analysis." Journal of Statistical Software, Vol. 16, Issue 10.

<sup>2</sup> Falmagne, J‐Cl, and J‐P. Doignon. "A class of stochastic procedures for the assessment of knowledge." British Journal of Mathematical and Statistical Psychology 41.1 (1988): 1-23.

### Učenik

Učenik može da:
- rešava testove koji su mu dodeljeni
- vidi rezultate testova koje je rešio
- vidi svoje stanje znanja

Test se rešava primenom vođenog testiranja, što znači postavljanjem jednog po jednog pitanja. Algoritam vođenog testiranja implementiran je na sledeći način: 

- Postavljanje prvog pitanja:

    Ukoliko je učenik prvi koji rešava dati test, sva pitanja imaju podjednaku šansu da budu postavljena kao prvo pitanje, pa se nasumično odabira jedno. 
    
    U suprotnom, ukoliko postoje učenici koji su već rešavali test, posmatramo probabilistički prostor znanja, u kom je svakom stanju znanja dodeljena verovatnoća. Verovatnoća je srazmerna broju učenika koji se nalaze u tom stanju znanja. Metodom oteženjenog nasumičnog izbora stohastički se bira problem tako što je težina vezana za problem izračunata kao suma verovatnoća svih stanja znanja u kojima je prisutan taj problem. Kada se odabere problem, studentu se postavlja pitanje koje je vezano za taj problem. Na ovaj način, veća je verovatnoća da se učeniku postavi pitanje na koje se od njega očekuje tačan odgovor.

- Postavljanje narednih pitanja

    Nakon odgovora na pitanje, u zavisnosti od toga da  li je odgovor tačan ili ne, verovatnoće pridružene svakom stanju se menjaju (u našem slučaju povećavaju ili smanjuju za 20% i nakon toga normalizuju tako da ukupna suma verovatnoća bude jednaka 1).
    
    Ukoliko je student odgovorio tačno na postavljeno pitanje, svim stanjima u kojima je uključen problem vezan za to pitanje se povećava verovatnoća, dok se ostalim stanjima smanjuje.

    U suprotnom, pri netačnom odgovoru, svim stanjima u kojima je uključen problem vezan za to pitanje se smanjuje verovatnoća, dok se ostalima povećava.

    Pri ovom postupku, pazi se da se učeniku ne postavi isto pitanje dva puta.

- Završetak testa
    
    U našem projektu, test se završava tek kada se studentu postave sva pitanja, iako je moguće da se zaključi u kom je stanju student i ranije, u opštem slučaju. Sistem bi mogao da se izmeni kako bi podržavao raniji završetak testa tako što bi se implementirao određeni uslov završetka, poput toga da se verodostojnost određenog stanja poveća iznad 0.8. Na ovaj način, ne bi bilo potrebno da student odgovori na sva pitanja, ali bi morala da se promeni implementacija računanja poena ostvarenih na testu, pošto na neka pitanja student ne bi davao odgovore a smatralo bi se da je savladao problem.


## Pokretanje

Potrebna okruženja: Node.js, Python 3.6 i sqlite3

### Preuzimanje paketa

U terminalu pozicionirati se na putanje *backend* i *frontend* i na svakoj pokrenuti komandu `npm install`. Pozicionirati se na putanju *ks* i ukoliko se koristi Python pokrenuti komandu `pip install -r requirements.txt` a ukoliko se koristi Conda pokrenuti komandu `conda install --file requirements.txt`.

### Pokretanje aplikacije

U terminalu pozicionirati se na putanju *ks* i pokrenuti komandu `flask run`. Pozicionirati se na putanje *backend* i *frontend* i na svakoj pokrenuti komandu `npm start`. U pretraživaču otići na adresu http://localhost:3000.

## Tim

Tim čine:
 * Miloš Radojčin, R2 15/2020 (@milosradojcin)
 * Nenad Mišić, R2 19/2020 (@nenad.misic)
